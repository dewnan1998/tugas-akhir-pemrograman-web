<?php

use Illuminate\Database\Seeder;
use App\Role;
class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        // Role::truncate();    

        $role = new Role();
        $role->role_name = 'Superadmin';
        $role->description= 'rules all system';
        $role->value = 4;
        $role->save();

        $role = new Role();
        $role->role_name = 'Admin';
        $role->description= 'can manage post,user';
        $role->value = 3;
        $role->save();

        $role = new Role();
        $role->role_name = 'Editor';
        $role->description= 'Can edit post';
        $role->value = 2;
        $role->save();

        $role = new Role();
        $role->role_name = 'Member';
        $role->description= 'regular member';
        $role->value = 1;
        $role->save();

        $role = new Role();
        $role->role_name = 'Banned';
        $role->description= "banned account can't login to system ";
        $role->value = 0;
        $role->save();
    }
}
