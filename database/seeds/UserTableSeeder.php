<?php

use Illuminate\Database\Seeder;

use App\Role;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $role = Role::where('role_name', 'Superadmin')->first();
        // error_log('testing');
        // error_log($role);
        // $role_manager  = Role::where('role_name', 'manager')->first();
        // $employee = new User();
        // $employee->name = 'Superadminstrator';
        // $employee->email = 'superadmin@dewworld.web.id';
        // $employee->username = 'superadmin';
        // $employee->avatar = '';
        // $employee->password = bcrypt('secret');
        // $employee->role()->associate($role);
        // $employee->save();

        User::create([
            'username'=>'dewnan98',
            'password'=> Hash::make('admin'),
            'name' => 'Dewa Nanda',
            'email'=>'dewnan1998@gmail.com',
            'avatar'=>'',
        ]);
        factory(App\User::class, 200)->create()->each(function($u) {
            $dewa = User::where('username','dewnan98')->firstOrFail();
            $u->follow($dewa);
          });

    }
}
