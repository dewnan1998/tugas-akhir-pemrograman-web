<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'name', 'email', 'password','username','avatar','provider','provider_id','role_id','cover_image','short_bio','token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function followers() 
    {
        return $this->belongsToMany(self::class, 'user_follows', 'follows_id', 'user_id')
                    ->withTimestamps();
    }

    public function follows() 
    {
        return $this->belongsToMany(self::class, 'user_follows', 'user_id', 'follows_id')
                    ->withTimestamps();
    }

    public function responses()
    {
        return $this->hasMany(Response::class);
    }

    public function respones()
    {
        return $this->hasMany(Response::class);
    }

    public function follow($userId) 
    {
        $this->follows()->attach($userId);
        return $this;
    }

    public function unfollow($userId)
    {
        $this->follows()->detach($userId);
        return $this;
    }


    public function isFollowing($userId) 
    {
        return (boolean) $this->follows()->where('follows_id', $userId)->first();
    }


    public function role()
    {
       return $this->belongsTo(Role::class);
    }

    public function isPostOwner( $post)
    {  
            error_log($post->user_id);
            
            return $this->id==$post->user_id;
    }

    public function isMe($username)
    {
        return $this->username==$username;
    }
    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function isAdminstrator()
    {
        return $this->role->value >=2 ;
    }

    public function isSuperadmin()
    {
        return $this->role->value ==4 ;
    }
    public function isAdmin()
    {
        return $this->role->value ==3 ;
    }

    public function isEditor()
    {
        return $this->role->value ==2 ;
    }
    
    public function isMember()
    {
        return $this->role->value ==1 ;
    }

    public function isBanned()
    {
        return $this->role->value ==0 ;
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    
}
