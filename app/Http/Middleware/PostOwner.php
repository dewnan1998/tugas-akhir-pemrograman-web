<?php

namespace App\Http\Middleware;

use Closure;
use App\Post;
use Illuminate\Support\Facades\Auth;
class PostOwner
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $post = Post::where('meta' , $request->route('meta'))->firstOrFail();
      
        if(Auth::check())
        {
            if( Auth::user()->isPostOwner($post)) return $next($request);
        }

        return redirect ('p/'.$post->meta);
       
    }
}
