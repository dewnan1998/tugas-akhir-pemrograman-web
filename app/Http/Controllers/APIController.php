<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
use Redirect;
class APIController extends Controller
{
    //

    public function form(Request $request)
    {
        
        
        Session::put('callback', $request->query('callback'));
        // Session::set);
     
        return view('api.login')->with('callback');
    }

    // public function login(Request $request)
    // {

    //     // dd($request->query('callback'));

    //     $request->validate([
    //         'username' => 'required|string',
    //         'password' => 'required|string',
    //     ]);

    //     if (Auth::attempt(['username'=>$request->username, 'password'=>$request->password]))
    //     {

            
    //         return Auth::user();
            
           
 
    //         return Redirect::to($request->query('callback'))->with('data' ,Auth::user());

    //     }

    //         return redirect()->back()->with('error' ,'Username Atau Password salah');

    // }


    
    public function __construct()
    {
        // $this->middleware('auth:api', ['except' => ['login' ,'form']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {

        // return 'sa'P
        $credentials = request(['username', 'password']);

        if (! $token = auth('api')->attempt($credentials)) {

            return redirect()->back();
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        // return $this->respondWithToken($token);

        // return Redirect::to($u);

        $token = $this->respondWithToken($token);
        $parse = json_decode($token->getContent());
        // dd($parse->access_token);
        
        return Redirect::to($request->query('callback').'?token='.$parse->access_token);


    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth('api')->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth('api')->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth('api')->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            // 'expires_in' => auth('api')->factory()->getTTL() * 60
            'expires_in' => auth('api')->factory()->getTTL() * 60
        ]);
    }


}
