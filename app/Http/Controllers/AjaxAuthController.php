<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AjaxAuthController extends Controller
{
    //
    public function login(Request $request)
    {
        error_log($request);
        $username = $request->username;
    	$password = $request->password;
     
    	// now we use the Auth to Authenticate the users Credentials
		// Attempt Login for members
		if (Auth::attempt(['username' => $username, 'password' => $password])) {
			$msg = array(
				'status'  => 'success',
				'message' => 'Login Successful'
			);
			error_log('berhasil login');
			return response()->json($msg);
		} else {
			$msg = array(
				'status'  => 'error',
				'message' => 'Login Fail !'
			);
			error_log('gagal login');
			return response()->json($msg);
			
		}
        error_log('end line');
		
	}
}
