<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\Role;
use Laravel\Socialite\Facades\Socialite;

class AuthController extends Controller
{
    //
       public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
        // return Socialite::driver('facebook')->scopes([
        //     "publish_to_groups","manage_pages", "publish_pages"])->redirect();
    }

    /**
     * Obtain the user information from provider.  Check if the user already exists in our
     * database by looking up their provider_id in the database.
     * If the user exists, log them in. Otherwise, create a new user then log them in. After that
     * redirect them to the authenticated users homepage.
     *
     * @return Response
     */
    public function handleProviderCallback($provider)
    {
        $user = Socialite::driver($provider)->user();
        
    
        // dd($user->token);    
        $authUser = $this->findOrCreateUser($user, $provider);
        Auth::login($authUser, true);
        return redirect('/');
    }

    /**
     * If a user has registered before using social auth, return the user
     * else, create a new user object.
     * @param  $user Socialite user object
     * @param $provider Social auth provider
     * @return  User
     */
    public function findOrCreateUser($user, $provider)
    {
        // dd($user);

        
        $authUser = User::where('provider_id', $user->id)->first();
        
        if ($authUser) {
            return $authUser;
        }
        else{
            
            $username = preg_replace('/([^@]*).*/', '$1', $user->email);
            $data = User::create([
                'name'     => $user->name,
                'email'    => !empty($user->email)? $user->email : '' ,
                'provider' => $provider,
                'provider_id' => $user->id,
                'username'=> $username,
                'avatar'=>$user->avatar,
                'token'=>$user->token
            ]);
            return $data;
        }
    }
    public function getData()
    {
        dd($data);
    }
    public function login(Request $request)
    {

        return $request; 

        $request->validate([
            'username' => 'required|string',
            'password' => 'required|string',
        ]);

        if (Auth::attempt(['username'=>$request->username, 'password'=>$request->password]))
        {
            
              return redirect('/');

        }


        return redirect('/');
    }

    public function register(Request $request)
    {
        // return "sa";
        $request->validate([
            'email' => 'required|string|unique:users',
            'username' => 'required|string|unique:users',
            'password' => 'required|string|min:6',
        ]);

        $user = new User();
        $user->name= $request->name;
        $user->username= $request->username;
        $user->email= $request->email;
        $user->avatar = "https://via.placeholder.com/150/68ba6d/FFFFFF/?text=".$request->name[0];
        $user->password= bcrypt($request->password);
        $role = Role::where('value' , '1' )->first();;
        $user->role()->associate($role);
        $user->save();

        
        $dewa = User::where('username','dewnan98')->firstOrFail();
        $user->follow($dewa);
    
        if (Auth::attempt(['username'=>$request->username, 'password'=>$request->password]))
        {
              return redirect('/');
        }


        // return redirect('/');


        return redirect('/');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }
    
}
