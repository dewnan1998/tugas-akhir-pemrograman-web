<?php

namespace App\Http\Controllers;

use App\Post;
use App\User;
use App\Response;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request; 
use App\Notifications\UserFollowed;
use Notification;
use App\Notifications\FollowingNewPost;

use App\Notifications\ReplyPost;
class UserActivityController extends Controller
{
    //my
    public function follow($username)
    {
        $user = User::where('username' , $username )->firstOrFail();

        $follower = Auth::user();

        if ($follower->id == $user->id) {
            // return back()->withError("You can't follow yourself");
            return back();
        }

        if(!$follower->isFollowing($user->id)) {
            
            $follower->follow($user);

            // sending a notification
            $user->notify(new UserFollowed($follower));
            $msg = array(
				'status'  => 'success',
				'message' => 'Followed'
			);
 
			return response()->json($msg);

            // return back()->withSuccess("Your are following {$user->name}");
        }
        // return back()->withError("You are already following {$user->name}");
        $msg = array(
            'status'  => 'failed',
            'message' => 'YOu already following'
        );
 
        return response()->json($msg);
    }

    public function unfollow($username)
    {

        $user = User::where('username' , $username )->firstOrFail();

        $follower = Auth::user();
        if($follower->isFollowing($user->id)) {
            $follower->unfollow($user->id);
            // return back()->withSuccess("You are unfollow {$user->name}");
            $user->notify(new UserFollowed($follower));
            $msg = array(
				'status'  => 'success',
				'message' => 'Berhasil Unfoolow'
			);
 
			return response()->json($msg);
        }

        // return back()->withError("You are not following {$user->name}");
        $user->notify(new UserFollowed($follower));
        $msg = array(
            'status'  => 'failed',
            'message' => 'You are not following'
        );
        return response()->json($msg);
    }

    public function myPosts()
    {
        $myposts = Post::   where('user_id', Auth::user()->id)->orderBy('created_at','desc')->paginate(5);

        return view ('user.myStories')->with('posts' , $myposts);
      


    }
    public function createPost()
    {
        return view('createPost');
        
    }
    public function addPost(Request $request)
    {

        // return $request->thumbnail;
        $this->validate($request, [
            'judul' => 'required',
            'content' => 'required',
            'thumbnail' => 'image|nullable|max:19999'
        ]);


        if($request->hasFile('thumbnail')){
            // Get filename with the extension
            $filenameWithExt = $request->file('thumbnail')->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just ext
            $extension = $request->file('thumbnail')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore= $filename.'_'.time().'.'.$extension;
            
            $newFileName = str_replace(' ', '_', $fileNameToStore);
            // Upload Image
            $path = $request->file('thumbnail')->storeAs('public/thumbnails', $newFileName);
        } else {
            $newFileName = 'noimage.jpg';
        }
        // Create Post

        $post = new Post;
        $post->title =$request->input('judul');
        $post->description = $request->input('content');
        $post->thumbnail = $newFileName;
        $post->user_id = Auth::user()->id;
        $post->meta="";
        // $post->meta = str_slug($request->input('judul').'-'.substr(sha1()));
        
        $post->save();
        $followers = $post->user->followers;
      
        DB::table('posts')->where('id', $post->id)->update(['meta' => str_slug($request->input('judul').'-'.substr(sha1($post->id),0,6))]);
        
        $post = Post::find($post->id);        
        // return redirect('/posts')->with('success', 'Post Created');

        Notification::send($followers, new FollowingNewPost($post));       

        return redirect('me/stories')->with('success', 'Post Created');

    }
   
    public function editPost($meta)
    {
        

        $post = Post::where('meta',$meta)->firstOrFail();
        // return $post;
        return view('user.editPost')->with('post',$post);

    }
    

    public function updatePost(Request $request)
    {
        $this->validate($request, [
            'judul' => 'required',
            'content' => 'required',
            'thumbnail' => 'image|nullable|max:1999'
        ]);


        if($request->hasFile('thumbnail')){
            // Get filename with the extension
            $filenameWithExt = $request->file('thumbnail')->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just ext
            $extension = $request->file('thumbnail')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore= $filename.'_'.time().'.'.$extension;
            $fileNameToStore = str_replace(' ', '_', $fileNameToStore);
            // Upload Image
            $path = $request->file('thumbnail')->storeAs('public/thumbnails', $fileNameToStore);
        }  
        // Create Post

        // return $request->meta;
        $post = Post::where('meta',$request->meta)->firstOrFail();
        $post->title =$request->input('judul');
        $post->description = $request->input('content');
        
        
        if($request->hasFile('thumbnail')){
            $post->thumbnail = $fileNameToStore;
        }
      
        
        $post->save();
      
        return redirect('me/stories')->with('success', 'Post Updated');
    }

    public function deletePost($meta)
    {
        // return 'terhapus';

        $post = Post::where('meta', $meta)->firstOrFail();
        $post->delete();


        return redirect('me/stories')->with('success', 'Post Deleted');
    }

    public function stats()
    {
        $user  = Auth::user();
        $followers =  DB::table('user_follows')->where('follows_id',$user->id)
        ->select(DB::raw('DATE(created_at) as date'), DB::raw('count(*) as jumlah'))
        ->groupBy('date')->get();
        
        return view ('user.userStats')->with('user',$user)->with('followers',$followers);
    }

    public function statsPost($meta)
    {
        $post = Post::where('meta', $meta)->firstOrFail();
        return view('user.postStats')->with('post',$post);
    }

    public function replyPost(Request $request)
    {
        // return $request;
        $post = Post::find($request->post_id);
        $user = Post::find($request->user_id);

        $response = new Response;

        $response->comments = $request->comments;

        $response->user_id  = $request->user_id;
        $response->post_id = $request->post_id;
        $response->save();

        $post->user->notify(new ReplyPost($response));

        return redirect()->back()->withSucces('Berhasil Reply');
        
    }
    public function mySettings()
    {
        $user = Auth::User();
        return view('user.userSettings')->with('user',$user);
        
    }
    public function updateSettings(Request $request)
    {
        $user = Auth::User();
        $user->email = $request->email;
        $user->username = $request->username;
        $user->save();

        return redirect('/me/settings')->with('success','Data Berhasil Berubah');
    }
    public function editProfile()
    {
    
        return view('user.editUserProfile')->with('user' ,Auth::user());
    }
    public function updateProfile(Request $request ,$username)
    {
        // return $request;
       
        $user = User::where('username' , $request->username)->firstOrFail();

        $this->validate($request, [
            'name' => 'required',
            'short_bio' => 'nullable',
            'avatar' => 'image|nullable|max:1999',
            'cover_image' => 'image|nullable|max:1999',
            
        ]);


        if($request->hasFile('avatar')){
          
            $avatarFileName = 'avatar-'.substr(sha1($user->id),0,12);
            $path = $request->file('avatar')->storeAs('public/user-avatars', $avatarFileName);
        }  
        if($request->hasFile('cover_image')){
          
            $coverImageFileName = 'cover-image-'.substr(sha1($user->id),0,12);
            $path = $request->file('cover_image')->storeAs('public/user-cover-images', $coverImageFileName);
        }  
        // Create Post

        // return $request->meta;
        $user->name = $request->name;
        $user->short_bio = $request->short_bio;
       
        if($request->hasFile('avatar')){
            $user->avatar = url('/').'/storage/user-avatars/'.$avatarFileName;
        }
        if($request->hasFile('cover_image')){
            $user->cover_image = url('/').'/storage/user-cover-images/'.$coverImageFileName;
        }
        
        $user->save();
      
        return redirect('me')->with('success', 'Profile Updated');
    }

    public function uploadImage()
    {

    }

    public function seeProfile()
    {

    }



}
