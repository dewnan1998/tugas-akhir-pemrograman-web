<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserProfileController extends Controller
{
    //

    public function index()
    {
        $user = User::where('username',$username)->firstOrFail();
        $posts = Post::where('user_id',$user->id)->orderBy('created_at','desc')->get();
        return view ('userProfile')->with(['user'=> $user , 'posts'=>$posts]);  
    }

    public function follows()
    {

    }

    public function followers()
    {
        
    }
    public function claps()
    {

    }
    public function responses()
    {

    }
    public function  stats()
    {

    }
}
