<?php

namespace App\Http\Controllers;

use App\User;
use App\Post;
use DB;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function index()
    {
        $posts= Post::orderBy('created_at' ,'desc')->get();
        // return $posts;
        return view('home')->with('posts',$posts);
    }

    public function showPost($get)
    {
        $post = Post::where('meta' ,$get)->firstOrFail();
        // $user = $post->user;
        $posts = Post::inRandomOrder()->get()->take(3);
        
        DB::table('posts')->whereId($post->id)->increment('views');
        return view('showPost')->with('post',$post)->with('posts',$posts);
        
    }

    public function register()
    {
        return view ('register');
    }
    public function login()
    {
        return view ('auth.login');
    }
    public function userProfile($username)
    {
        $user = User::where('username',$username)->firstOrFail();
        $posts = Post::where('user_id',$user->id)->orderBy('created_at','desc')->get();
        return view ('userProfile')->with(['user'=> $user , 'posts'=>$posts]);
        
    }
    
}
