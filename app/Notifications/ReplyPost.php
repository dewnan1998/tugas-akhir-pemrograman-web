<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ReplyPost extends Notification
{
    use Queueable;

    protected $response;

    public function __construct($response)
    {
        $this->response = $response;
    }

    public function via($notifiable)
    {
        return ['database'];
    }

    public function toDatabase($notifiable)
    {

        $img_src = !empty($this->response->user->avatar)? '/storage/avatar/'.$this->response->useravatar : 'https://via.placeholder.com/150/68ba6d/FFFFFF/?text='.$this->response->user->name[0];
     
            return [
            'response_id' => $this->response->id,
            'message'=> '<strong>'. $this->response->user->name.'</strong> Reply to your post '.'"'.strip_tags(substr($this->response->comments,0,30)).'"',
            'links'=> '/p/'.$this->response->post->meta,
            'img_src'=> $img_src,
        ];
    }
}
