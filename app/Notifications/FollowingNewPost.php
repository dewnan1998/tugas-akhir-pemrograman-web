<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class FollowingNewPost extends Notification
{
    use Queueable;

    protected $post;

    public function __construct($post)
    {
        $this->post = $post;
    }

    public function via($notifiable)
    {
        return ['database'];
    }

    public function toDatabase($notifiable)
    {

        $img_src = !empty($this->post->user->avatar)? '/storage/avatar/'.$this->post->user->avatar : 'https://via.placeholder.com/150/68ba6d/FFFFFF/?text='.$this->post->user->name[0] ;
     
            return [
            'post_id' => $this->post->id,
            'message'=> '<strong>'. $this->post->user->name.'</strong> Created new Post'.'"'.strip_tags(substr($this->post->title,0,20)).'"',
            'links'=> '/p/'.$this->post->meta,
            'img_src'=> $img_src,
        ];
    }
}
