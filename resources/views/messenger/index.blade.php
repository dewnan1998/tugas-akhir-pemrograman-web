<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="shortcut icon" type="image/png" href="{{asset('img/logo.png')}}"/>
	<title>Dew World</title>
	<link rel="stylesheet" href="{{ asset('css/app.css')}}">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    {{-- <script src="{{ asset('js/app.js') }}"></script> --}}
	<style>
		.container{
			margin: auto;
			margin-top: -80px;
			box-shadow: 0px 1px 5px 0px #68a2ff;
			background: white;
			border-radius: 3px;
		}
		.navbar{
			margin: auto;
		}
		.anyClass {
			height:500px;
			overflow-y: scroll;
		}
		ul{
			list-style: none;
		}
		#style-scroll::-webkit-scrollbar-track
		{
			-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
			border-radius: 10px;
			background-color: #fff;
		}
		#style-scroll::-webkit-scrollbar
		{
			width: 5px;
			background-color: #93bdff;
		}
		#style-scroll::-webkit-scrollbar-thumb
		{
			border-radius: 10px;
			-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
			background-color: #93bdff;
		}
		/* Message Chat box style */
		.received_msg {
			display: inline-block;
			padding: 0 0 0 10px;
			vertical-align: top;
			width: 92%;
		}
		.received_withd_msg p {
			background: #f2f2f2 none repeat scroll 0 0;
			border-radius: 0px 17px 17px 17px;
			color: #646464;
			font-size: 14px;
			margin: 0;
			padding: 10px 10px 10px 15px;
			width: 100%;
		}
		.time_date {
			color: #747474;
			display: block;
			font-size: 12px;
			margin: 8px 0 0;
		}
		.received_withd_msg { width: 57%;}
		.mesgs {
			float: left;
			padding: 30px 15px 0 25px;
			width: 60%;
		}
		.sent_msg p {
			background: #d1e3ff none repeat scroll 0 0;
			border-radius: 17px 0px 17px 17px;;
			font-size: 14px;
			margin: 0; color:#000;
			padding: 10px 10px 10px 15px;
			width:100%;
		}
		.outgoing_msg{ overflow:hidden; margin:26px 0 26px;}
		.sent_msg {
			float: right;
			width: 46%;
		}
		
	</style>
</head>
<body>
	<div class="jumbotron" style="background: #232323"></div>
	<div class="container row p-3" id="chatbox">
		<div class="col-md-3">
			<nav class="navbar navbar-expand-sm p-3 d-flex flex-column mb-3">
				{{-- <h4>BaChat List</h4> --}}
                {{-- <img src="{{ asset('img/logo.png') }}" alt="" style="height:60px" class="mb-3"> --}}
                <a class="navbar-brand" href="/"><img src="{{asset('img/core-img/dew-world-logo-black.png')}}" alt="Logo" style="max-width : 150px;"></a>

				<a href="/">
					<img src="{{ asset('img/BaChat2.png') }}" alt="" style="height:30px">
				</a>
            </nav>
            
			<ul class="list-group">
            
                @foreach ($channels as $data)
					<a @click.prevent="getMessage({{$data->id}})" style="color:black">
						<li class="list-group-item mb-2">
							<span><img class="img-fluid rounded-circle mr-2" src="https://cdn.pixabay.com/photo/2016/09/01/17/18/profile-1636642_960_720.png" alt="" style="height:25px;"></span><b>{{ $data->nama }}</b>
						</li>
                    </a>
                @endforeach
                
                
			</ul>
		</div>
		<div class="col-md-9">
			<nav class="navbar navbar-expand-sm p-3 float-right">
				<a href="/logout" style="color:#68a2ff"  class="float-right">
					{{-- <b>{{ Auth::User()->name }} : Logout</b> --}}
				</a>
			</nav>
			<br><br><br>
			<div class="form-group">
				<ul class="anyClass" id="style-scroll">
                    
					<li class="nav-item" v-for="message in messages">
						<template v-if="message.user_id === user_id">
							<div class="outgoing_msg mr-3">
								<div class="sent_msg">
									<p><b>@{{ message.user }}</b><br>
										@{{ message.message }}</p>
									<span class="time_date text-right">@{{ message.created_at }}</span>
								</div>
							</div>
						</template>
						<template v-else>
							<div class="incoming_msg mt-4">
								<div class="received_msg">
									<div class="received_withd_msg">
									<p><b>@{{ message.user }}</b><br>
										@{{ message.message }}</p>
									<span class="time_date text-right">@{{ message.created_at }}</span>
								</div>
								</div>
							</div>
						</template>
					</li>


				
				{{-- @foreach ( $mes as $data )
				<li class="nav-item">
					@if ( $data->user_id === Auth::User()->id )
						<div class="align-self-start p-1 mb-2 my-1 mx-3 rounded shadow-sm" style="background: #e2f1ff">
					@else
						<div class="align-self-start p-1 mb-2 my-1 mx-3 rounded shadow-sm" style="background: #fcfcfc">
					@endif
							<div class="small font-weight-bold text-primary">
								{{ $data->user->name }}
							</div>
							<div class="d-flex flex-row">
								{{ $data->message }}
								<div class="time ml-auto small text-right flex-shrink-0 align-self-end text-muted" style="width:75px;">
									{{ $data->created_at }}
								</div>
							</div>
						</div>
				</li>
				
				@endforeach --}}


				</ul>
			</div>
			<div class="w-100">
				{{-- <form action="/message/new" method="post">
				@csrf --}}
					{{-- <input type="hidden" name="kelompok_id" value="{{ $mes->kelompok->id }}"> --}}
					<div class="input-group mt-3">
						<input v-model='message' @keyup.enter="send" type="text" class="form-control mr-2" placeholder="Ketik Pesan ..." name="message" required autocomplete="off">
						<button @click.prevent="send" type="button" class="btn btn-default" style="border: 1.5px solid #dddddd"><i class="fa fa-paper-plane"></i></button>
					</div>
				{{-- </form> --}}
			</div>
		</div>
	</div>

    <script src="{{ asset('js/app.js') }}"></script>
    
	<script src="{{ asset('js/datastore.js') }}"></script>
	<script src="{{ asset('js/date-utils.js')}}"></script>

	{{-- <script src="https://cdn.jsdelivr.net/npm/vue-chat-scroll/dist/vue-chat-scroll.min.js"></script> --}}
	
	<script>
		// document.getElementById('style-scroll').scrollTop = 9999999;
		
		// Scroll Bar Chat nya
		// setInterval(updateScroll,400);
		// var scrolled = false;
		// function updateScroll(){
		// 	if(!scrolled){
		// 		var element = document.getElementById("style-scroll");
		// 		element.scrollTop = element.scrollHeight;
		// 	}
		// }
		// $("#style-scroll").on('scroll', function(){
		// 	scrolled = true;
		// });


		const chatbox = new Vue({
			el: '#chatbox',
			data: {
				messages:{},
				kelompok_id: '',
				message: '',
            
				// cek_id: '',
				user_id : {!! $user->id!!},

			},
			mounted(){
				// this.getMessage()
				// console.log('mantulll Cuyyyyyy -> getMessage()')
				this.listen()
			},
			methods: {
				listen() {
					// console.log('test listen')
					// Echo.channel('chats')
					// 	.listen('ChatEvent', (e) => {
					// 		console.log(e)
					// 		console.log("test pusher")
					// 	})
                        // console.log('cpk');

                },
				send(){
                    console.log('sasa')
					axios.post('http://bachat.kuy.web.id/api/sendMessage',{

						message : this.message,
						kelompok_id : this.kelompok_id,
                        user_id : this.user_id,

					})
					.then((response)=>{
						this.messages.push(response.data);
						this.message = '';
						this.getMessage(this.cek_id);
					})  
					.catch(function(error)
					{
						console.log(error)
					})
				},
				getMessage($id)
				{
					axios.get('http://bachat.kuy.web.id/api/message/'+$id,{
                        headers: {"Access-Control-Allow-Origin": "*"},

                    })
					.then((response)=>{
						console.log(response.data)
						this.messages = response.data
						this.kelompok_id = $id
					})
					.catch(function(error)
					{ 
						console.log(error)
					})
					
					console.log('Muncul');
					this.cek_id = $id;
						// this.scroll()
				},
				// scroll () {
				// 	window.onscroll = () => {
				// 		let bottomOfWindow = Math.max(window.pageYOffset, document.documentElement.scrollTop, document.body.scrollTop) + window.innerHeight === document.documentElement.offsetHeight
				// 		if (bottomOfWindow) {
				// 			this.scrolledToBottom = true // replace it with your code
				// 		}
				// 	}
				// },
			},
		});
	</script>
</body>
</html>