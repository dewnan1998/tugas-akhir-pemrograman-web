@extends('layouts.app')

@section('styles')
<meta name="csrf-token" content="{{ csrf_token() }}">

<style>
a{
    cursor: pointer;
 }
 
</style>

@endsection


@section('content')
<div id="follow_div_author" >
    @if(empty($user->cover_image))
    <div class="hero-area height-400 bg-img background-overlay" style="background-image: url(http://thealphagroup.net.au/wp-content/uploads/2015/04/blue-background.jpg);">
    
    @else
    <div class="hero-area height-400 bg-img background-overlay" style="background-image: url(/storage/user-cover-images/{{$user->cover_image}});">
    @endif
        <div class="container h-100">
        <div class="row h-100 align-items-center justify-content-center">
            <div class="col-12 col-md-8 col-lg-6">
                <div class="single-blog-title text-center">
                    <!-- Catagory -->
                 </div>
            </div>
        </div>
    </div>
</div> 


<div id="follow_div" class="container text-center" style="position: relative; bottom: 70px; z-index: 100">
   
    <img class=" " src="
    @if( empty($user->avatar))
    https://via.placeholder.com/150/68ba6d/FFFFFF/?text={{$user->name[0]}}
    @else
        {{-- /storage/user-avatars/{{$user->avatar}} --}}
        {{$user->avatar}}
    @endif
   " alt="" style="width :130px;  height: 130px; object-fit: cover;border-radius: 70px" class="ml-5">
    
    
    <h3 class="mt-3" style="font-family:KievitBold; opacity: 0.8;  ">{{$user->name}}</h3>
    <p>{{$user->short_bio}}</p>
    

    @if(Auth::check())

        @if(!Auth::user()->isMe($user->username))
            <div  class="d-inline-flex mb-4">
            @if(!Auth::user()->isFollowing($user->id))

            <form id="#followForm"action="" onsubmit="return false"; method='post' >
                @csrf
                <input id="usernameFollow"type="hidden" name="username" value="{{$user->username}}">
                <a 
                {{-- href="/{{'@'.$user->username}}/follow" --}} 
                    onclick="follow('{{$user->username}}')"
                  class="p-2 ml-4" style="border: 1px #32c95f solid; padding: 1px; border-radius : 5px;
                color:#32c95f">Follow
                </a>
            </form>

            @else
            <form id="#unfollowForm" action="" onsubmit="return false"; method='post' >
                    @csrf
                    <input id="usernameUnfollow"type="hidden" name="username" value="{{$user->username}}">

            <a onclick="unfollow('{{$user->username}}')" class="p-2 ml-4" style=" padding: 1px; border-radius : 5px;
                        color:white; background-color: #333">Following
                    </a>
                
                </form>
           
            @endif
            

          
            <form id=""  action="/messenger/chat" method='post' >
                    @csrf
                    <input id="usernameFollow"type="hidden" name="username" value="{{$user->username}}">
                    <button type="submit" 
                    {{-- href="/{{'@'.$user->username}}/follow" --}} 
                    class="p-2 ml-4" style="border: 1px #333 solid; padding: 1px; border-radius : 5px;
                    color:#333" >Send Message
                    </button>
                </form>

                

                    
                      
            <div class="btn-group dropdown m-0 p-0" style="">
                <button style="background: none; color: gray;border: none;"class=" p-0 btn btn-secondary btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fa fa-caret-down ml-2" aria-hidden="true"></i>           
                </button>
                <div class="dropdown-menu dropdown-menu-left " style="">
                    <a class="dropdown-item" href="#">Block User</a>
                    <a class="dropdown-item" href="#">Report User</a>
                        
                </div>
            
            </div>
        </div>
        @else
        <div class="mb-2"> 
            <a class="p-1 border" href="/{{'@'.Auth::user()->username}}/edit">Edit profile</a>
        </div>
        @endif
   
    @endif

    <br>
    <small class="mt-5"><a href="#">{{count($user->follows)}} Following</a> &nbsp <a href="#">{{count($user->followers)}} Followers</a></small>
</div>
<div class="container p-2 pt-0 mb-5 col-12 col-md-8 col-lg-7 ">
@include('inc.messages')
{{--     --}}


<ul class="nav nav-tabs" role="tablist">
   <li class="nav-item">
      <a class="nav-link active" href="#profile" role="tab" data-toggle="tab">Profile</a>
    </li> 
    <li class="nav-item">
      <a class="nav-link" href="#buzz" role="tab" data-toggle="tab">Claps</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="#buzz" role="tab" data-toggle="tab">Responses</a>
      </li>
    <li class="nav-item">
      <a class="nav-link" href="#followings" role="tab" data-toggle="tab">Following</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#followers" role="tab" data-toggle="tab">Followers</a>
    </li>
  </ul>
  
  <!-- Tab panes -->
  <div class="tab-content pt-3">
    @include('inc.lastestPost')
    @include('inc.follows')
    @include('inc.followers') 
   
  </div>
</div>

<script>
        function follow( username){
        
        
            // var _username = $('#usernameFollow').val();
            var _username = username;
        $.ajax({
        
                type:"POST",
                url:'/@'+_username+'/follow',
                                  
                data:{
                    username : _username
                },
                headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
                success:function(responsedata){
                        
                    console.log(responsedata);
                    $("#follow_div_author").load(location.href+" #follow_div_author>*","");
        
                }
             })
        
        }
        function unfollow( username){
        
        
        var _username = username
        // var _username = $('#usernameFollow').val();

        $.ajax({
        
                type:"POST",
                url:'/@'+_username+'/unfollow',
                                  
                data:{
                    username : _username
                },
                headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
                success:function(responsedata){
                        
                    console.log(responsedata);
                    $("#follow_div_author").load(location.href+" #follow_div_author>*","");
        
                }
             })
        
        }
</script>

<script>
    
$(function() { 
    // for bootstrap 3 use 'shown.bs.tab', for bootstrap 2 use 'shown' in the next line
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        // save the latest tab; use cookies if you like 'em better:
        localStorage.setItem('lastTab', $(this).attr('href'));
    });

    // go to the latest tab, if it exists:
    var lastTab = localStorage.getItem('lastTab');
    if (lastTab) {
        $('[href="' + lastTab + '"]').tab('show');
    }
});
</script>

</div>
<!-- ********** Hero Area End ********** -->
    



@endsection