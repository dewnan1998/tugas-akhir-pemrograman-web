<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login Account</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="icon" type="image/png" href="{{asset('img/core-img/d-logo2.png')}}"/>

	<link rel="stylesheet"  href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}">
	<link rel="stylesheet"  href="{{asset('fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
	<link rel="stylesheet"  href="{{asset('fonts/iconic/css/material-design-iconic-font.min.css')}}">
	<link rel="stylesheet"  href="{{asset('vendor/animate/animate.css')}}">
	<link rel="stylesheet"  href="{{asset('vendor/css-hamburgers/hamburgers.min.css')}}">
	<link rel="stylesheet"  href="{{asset('vendor/animsition/css/animsition.min.css')}}">
	<link rel="stylesheet"  href="{{asset('vendor/select2/select2.min.css')}}">
	<link rel="stylesheet"  href="{{asset('vendor/daterangepicker/daterangepicker.css')}}">
	<link rel="stylesheet"  href="{{asset('css/util.css')}}">
	<link rel="stylesheet"  href="{{asset('css/login.css')}}">

	{{-- <script src="{{asset('js/jquery/jquery-2.2.4.min.js')}}"></script> --}}

</head>
<body>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100" style="">
				{{-- <div id="error-message" class="alert alert-danger" style="display : none">
					<p> Username atau password salah</p> 
				</div> --}}
			 
			 
				<span class="login100-form-title p-b-26">
					Sign in
				</span>
				<span class="login100-form-title p-b-48">
					<img src="{{asset('img/core-img/d-logo2.png')}}" alt="" style="height: 70px; max-width: 70px; border-radius: 10px;">
				</span>

				<form   id="login-form"class="login100-form validate-form" method="post" action="/api/auth/login?callback={{Session::get('callback')}}">
					
				@csrf
					<div class="wrap-input100 validate-input" data-validate =" Enter Username">
						<input class="input100" type="text" name="username">
						<span class="focus-input100" data-placeholder="Username"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Enter password">
						<span class="btn-show-pass">
							<i class="zmdi zmdi-eye"></i>
						</span>
						<input class="input100" type="password" name="password">
						<span class="focus-input100" data-placeholder="Password"></span>
					</div>

					<div class="container-login100-form-btn">
						<div class="wrap-login100-form-btn">
							<div class="login100-form-bgbtn"></div>
							<button onclick="form_submit()"  type="submit"class="login100-form-btn edt-btn">
								Login
							</button>
						</div>
					</div>
				</form>

				<div class="text-center p-t-115">
					<span class="txt1">
						Don’t have an account?
					</span>

					<a class="txt2" href="/register"  style="">Sign Up</a>
				</div>
			
		</div>
	</div>
	{{-- <div class="modal-footer">
	  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	  <button type="button" class="btn btn-primary">Send message</button>
	</div> --}}
  </div>
</div>
</div>




		</div>
	</div>
	

	<div id="dropDownSelect1"></div>
	
	<script src="{{asset('vendor/jquery/jquery-3.2.1.min.js')}}"></script>
 	
	<script src="{{asset('js/login.js')}}"></script>
 

</body>
</html>