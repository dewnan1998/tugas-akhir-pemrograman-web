@extends('layouts.app')

@section('styles')
<link rel="stylesheet" type="text/css" href="/post/store">
<script src="https://cdn.jsdelivr.net/npm/medium-editor@latest/dist/js/medium-editor.min.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/medium-editor@latest/dist/css/medium-editor.min.css" type="text/css" media="screen" charset="utf-8">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <!-- Style CSS -->
<style type="text/css">

    .medium-editor-placeholder:after {
    text-align: center;
    width:100%;
    opacity: 0.6;
    }
    .label-image
    {
        position: relative; bottom: 100px; color: white

    }
    .label-image:hover
    {
        cursor: pointer;
    }
    h1{
        font-size: 50px;
    }
    h1, h2, h3
    {
        opacity: 1;
            font-family: KievitBold !important;
    }
    p{
        font-weight: normal;
            opacity: 0.8;
            font-family: Charter;
    }
</style>



@endsection


@section('content')


<div id="bg"class="hero-area height-600 bg-img background-overlay" style="background-image: url(https://loremflickr.com/320/240);">
    <div class="container h-100">
        <div class="row h-100 align-items-center justify-content-center">
            <div class="col-12 col-md-8 col-lg-6">
                <div class="single-blog-title text-center">
                    <!-- Catagory -->
                   
                    <h3 id="title"class="editable-tittle"></h3>
                </div>
            </div>
        
        </div>
        <div class="form-group upload-image">
    
        
       
        
<form  enctype="multipart/form-data" id="form" method="post" action="{{action('UserActivityController@addPost') }}" >
    @csrf

    <input id="titleInput" type="text" name="judul" style="display: none">
    <textarea id="contentInput" name="content" style="display: none"></textarea>
    
    <label class="label-image float-right"for="imageInput" style=""> <h6 style="color: white; position: relative; top: -30px;"><i class="fas fa-images"></i>Upload Thumbnail <h6></label>

        <input data-preview="#preview" name="thumbnail" type="file" id="imageInput" onchange="readURL(this)"
            style="display: none"
        >


</form>

@include('inc.messages')
 

        </div>
    </div>

</div>
<script>
    var editor = new MediumEditor('.editable-tittle', {
    keyboardCommands: false,    
    toolbar: false,
    disableReturn:true,
    maxLength : 20,

    placeholder: {
        text: 'Title '
    }
    });
</script>
<!-- ********** Hero Area End ********** -->

<div class="main-content-wrapper section-padding-100">
    <div class="container">
        <div class="row justify-content-center">
            <!-- ============= Post Content Area ============= -->
            <div class="col-12 col-lg-8">
                <div class="single-blog-content mb-100">
                    <!-- Post Meta -->
                  
                    <!-- Post Content -->


                   <style type="text/css">
                       
                            .editor *{
                                        color: black;

                            }
                            .editor p
                            {
                                font-size: 20px;
                            }

                   </style>
                   
                    <div id="content" class="post-content editor" style="color: black">
                       
                        <!-- Post Tags -->
                    
                        <!-- Post Meta -->
                      
                    </div>
                </div>
                <div class="container float-right" style="opacity: 0.6">
               

                <a id="save"> <h4 class="float-right" style="border: 1px solid black; border-radius: 20px; padding: 8px;">
                   <i class="fas fa-plus-circle"></i> Publish
                </h4>
                </a>

                <a href=""> <h4 class="float-right mr-3" style="border: 1px solid #7397d1; border-radius: 20px; padding: 8px;color: #7397d1">
                    <i class="fas fa-save"></i> Draft
                </h4>
                </a>
                </div>
            </div>

        </div>

    </div>

</div>


<script type="text/javascript">

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#bg')
                .css("background-image", 'url('+e.target.result+')')


                     console.log("url : "+e.target.result);  
             };



        reader.readAsDataURL(input.files[0]);
    }
}

var editor2 = new MediumEditor('.editor', {
// options go here
         toolbar: {
                    buttons: ['bold', 'italic', 'underline', 'strikethrough', 'quote', 'anchor', 'image', 'justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull', 'superscript', 'subscript', 'orderedlist', 'unorderedlist', 'pre', 'removeFormat', 'outdent', 'indent', 'h2', 'h3', 'html'],
                },
                buttonLabels: 'fontawesome',
                anchor: {
                    targetCheckbox: true
                },
        
             placeholder: {
    text: 'Tell your story...'
        }

});

 
$('#save').click(function () {
    var title = $('#title').html();
    var content = $('#content').html();

    $('#titleInput').val(title);
    $('#contentInput').val(content);

    $("#form").submit();
    // $('#hiddeninput').append(mysave);
    // alert($('#hiddeninput').val());
});

 

</script>

@endsection