<div role="tabpanel" class="tab-pane fade in active show" id="profile"  style="z-index: 1000">

        <div class=" mt-4 container-fluid">

            @if(count($posts)>0)
                <h5>Latest</h5>
            
                @foreach ($posts as $post )
                     <div class="tile shadow container p-3 mb-5 ">
                        <div class="header mb-0">
                                @include('inc.author')
                        </div>
                    <a href="/p/{{$post->meta}}"><div class="banner w-100 mb-4 mt-4">
                            <img  class="w-100" src="/storage/thumbnails/{{$post->thumbnail}}" alt="" style="object-fit:cover ;height: 200px">
                        </div>
                    </a>
                
                        <div class="content mt-0"> 
                                <a href="/p/{{$post->meta}}">    
                                    <h2 style="opacity: 0.8;font-family: KievitBold !important">{{ucwords($post->title)}}</h2>
                                </a>    
                                <p class="card-text">{{
                                        strip_tags(substr($post->description,0,400))}}
                                        @if(strlen($post->description) >400)
                                            {{ ' ...'}}
                                        @endif</p>
                        </div>
                        
        
                    </div>
                    
                @endforeach
            @else
                    <div class="center align-center mx-auto ">
                            <h4 class="mx-auto" style="opacity: 0.6;">This user has no post</h4>
                    </div>
            @endif
             
        </div>

    </div>