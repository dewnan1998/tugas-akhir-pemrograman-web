<div role="tabpanel" class="tab-pane fade" id="followers">
        
        @if(count($user->followers)>0)
        <h6 class="mb-4" style="opacity: 0.8">{{$user->name}} is followed by</h6>
            @foreach ($user->followers as $follower )
            <div class="row border p-2 mb-3">
                    <div class="col-2">
                            <img class=" " src="
                            @if( empty($follower->avatar))
                            https://via.placeholder.com/150/68ba6d/FFFFFF/?text={{$follower->name[0]}}
                            @else
                                /storage/user-avatars/{{$follower->avatar}}
                            @endif
                           " alt="" style="height :70px;width: 70px;border-radius: 70px" class="ml-2">
                    </div>
                      
                    <div class="col-10">
                         <a href="/{{'@'.$follower->username}}">
                            <h6 class="">
                                    {{$follower->name}}
                                </h6>
                            </a>
                                
                            <p>{{$follower->short_bio}}</p>
                              
                            @if(Auth::check())

        @if(!Auth::user()->isMe($follower))
            <div  class="d-inline-flex mb-4">
            @if(!Auth::user()->isFollowing($follower->id))

            <form id="#followForm"action="" onsubmit="return false"; method='post' >
                @csrf
                <input id="usernameFollow"type="hidden" name="username" value="{{$follower->username}}">
                <a 
                {{-- href="/{{'@'.$follower->username}}/follow" --}} 
                    onclick="follow('{{$follower->username}}')"
                  class="" style="
                color:#32c95f">Follow
                </a>
            </form>

            @else
            <form id="#unfollowForm" action="" onsubmit="return false"; method='post' >
                    @csrf
                    <input id="usernameUnfollow"type="hidden" name="username" value="{{$follower->username}}">

                    <a onclick="unfollow('{{$follower->username}}')" class="" style=" 
                        color:whit#333;">Following
                    </a>
                
                </form>
           
            @endif
        </div>
        @endif
   
    @endif    
                    </div>
            </div>        
            @endforeach
            @else
            <div class="center align-center mx-auto ">
                    <h4 class="mx-auto" style="opacity: 0.6;">This user has no followers</h4>
            </div>
        @endif
       
        
    </div>