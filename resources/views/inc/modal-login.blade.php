<style>
    
    .login-modal *
    {
        font-size: 14px !important;
    }
</style>

<link rel="stylesheet"  href="{{asset('css/login.css')}}">

<div id="login-modal" class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Sign in</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
                <div id="error-message" class="alert alert-danger" style="display : none">
                        <p> Username atau password salah</p> 
                    </div>
                <div class="wrap-login100" style="width: auto">
                 
                    <span class="login100-form-title p-b-26">
                        Sign in
                    </span>
                    <span class="login100-form-title p-b-48">
                        <img src="{{asset('img/core-img/d-logo2.png')}}" alt="" style="height: 70px; max-width: 70px; border-radius: 10px;">
                    </span>

                    <form id="login-form"class="login100-form validate-form" method="post" action="/login">
                        
                    @csrf
                        <div class="wrap-input100 validate-input" data-validate =" Enter Username">
                            <input class="input100" type="text" name="username">
                            <span class="focus-input100" data-placeholder="Username"></span>
                        </div>

                        <div class="wrap-input100 validate-input" data-validate="Enter password">
                            <span class="btn-show-pass">
                                <i class="zmdi zmdi-eye"></i>
                            </span>
                            <input class="input100" type="password" name="password">
                            <span class="focus-input100" data-placeholder="Password"></span>
                        </div>

                        <div class="container-login100-form-btn">
                            <div class="wrap-login100-form-btn">
                                <div class="login100-form-bgbtn"></div>
                                <button onclick="form_submit()"  type="submit"class="login100-form-btn edt-btn">
                                    Login
                                </button>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6 text-center ">
                                <a href="{{ url('/auth/google') }}" class="btn w-100 btn-google"><i class="fa fa-google"></i> Login with Google</a>
                                {{-- <a href="{{ url('/auth/twitter') }}" class="btn btn-twitter"><i class="fa fa-twitter"></i> Twitter</a> --}}
                                <a href="{{ url('/auth/facebook') }}" class="btn w-100 btn-facebook"><i class="fa fa-facebook"></i> Facebook</a>
                            </div>
                        </div>
                        
                    </form>

                    <div class="text-center p-t-115">
                        <span class="txt1">
                            Don’t have an account?
                        </span>
 
                        <a class="txt2" href="/register"  style="">Sign Up</a>
                    </div>
                
            </div>
        </div>
        {{-- <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Send message</button>
        </div> --}}
      </div>
    </div>
  </div>

 
    

  <script type="text/javascript">
    function form_submit() {
        if(LoginUser()) 
        {
            console.log("masuk pak eko");
            document.getElementById("login-form").submit()
        };
     }      

     function LoginUser()
        {
            
            
            var token    = $("input[name=_token]").val();
            var username    = $("input[name=username]").val();
            var password = $("input[name=password]").val();
            var data = {
                username:username,
                password:password
            };
            // Ajax Post 
            $.ajax({
                type: "post",
                url: "/login/user",
                data: data,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                cache: false,

                success: function (data)
                {
                    // consoe.log()
                    
                    console.log('login request sent !');
                    console.log('status: ' +data.status);
                    console.log('message: ' +data.message);

                    if(data.status=='success')
                    {
                        location.href = "/";
                    }
                    else{
                        $('#error-message').css({"display" : "block"});
                    }
                },
                error: function (data){
                    // alert('Fail to run Login..');
                    // location.href = "/";
                }
            });
            return false;
        }

    </script>

<script src="{{asset('js/login.js')}}"></script>   