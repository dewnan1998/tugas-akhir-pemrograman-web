<div class=" mt-4" style="width:350px" >
    <div class="row">
        <div class="col-3  ">
                <img class=" " src="
                @if( empty($post->user->avatar))
               https://via.placeholder.com/150/68ba6d/FFFFFF/?text={{ucwords($post->user->name[0])}}
                @else
                    {{$post->user->avatar}}
                @endif
               " alt="" style="width :70px;height: 70px; object-fit: cover;border-radius: 70px" class="ml-5">
        </div>


        <div class="col-9 mb-0">
            
            <a href="/{{'@'.$post->user->username}}">{{$post->user->name}} </a>
                @if(Auth::check())

                @if(!Auth::user()->isMe($post->user))
                    <div class="d-inline-flex mb-1">
                    @if(!Auth::user()->isFollowing($post->user->id))
        
                    <form id="#followForm"action="" onsubmit="return false"; method='post' >
                        @csrf
                        <input id="usernameFollow"type="hidden" name="username" value="{{$post->user->username}}">
                        <a 
                        {{-- href="/{{'@'.$user->username}}/follow" --}} 
                            onclick="followInAuthor()"
                          class="p-2 ml-4" style="border: 1px #32c95f solid; padding: 1px; border-radius : 5px;
                        color:#32c95f">Follow
                        </a>
                    </form>
        
                    @else
                    <form id="#unfollowForm" action="" onsubmit="return false"; method='post' >
                            @csrf
                            <input id="usernameUnfollow"type="hidden" name="username" value="{{$post->user->username}}">
        
                            <a onclick="unfollowInAuthor()" class="p-2 ml-4" style=" padding: 1px; border-radius : 5px;
                                color:white; background-color: #333">Following
                            </a>
                        
                        </form>
                   
                    @endif
        
                            
                              
                    <div class="btn-group dropdown m-0 p-0" style="">
                        <button style="background: none; color: gray;border: none;"class=" p-0 btn btn-secondary btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-caret-down ml-2" aria-hidden="true"></i>           
                        </button>
                        <div class="dropdown-menu dropdown-menu-left " style="">
                            <a class="dropdown-item" href="#">Block User</a>
                            <a class="dropdown-item" href="#">Report User</a>
                                
                        </div>
                    
                    </div>
                </div>  
                @endif
           
            @endif
            
        <br><small>{{$post->created_at->format('M j, Y')}} . {{$post->created_at->diffForHumans()}}</small></div>
    </div>
</div>


<script>
    
        function followInAuthor(){
        
        
        var _username = $('#usernameFollow').val();
        $.ajax({
        
                type:"POST",
                url:'/@'+_username+'/follow',
                                  
                data:{
                    username : _username
                },
                headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
                success:function(responsedata){
                        
                    console.log(responsedata);
                    $("#follow_div_author").load(location.href+" #follow_div_author>*","");
        
                }
             })
        
        }
        function unfollowInAuthor(){
        
        
        var _username = $('#usernameUnfollow').val();
        $.ajax({
        
                type:"POST",
                url:'/@'+_username+'/unfollow',
                                  
                data:{
                    username : _username
                },
                headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
                success:function(responsedata){
                        
                    console.log(responsedata);
                    $("#follow_div_author").load(location.href+" #follow_div_author>*","");
        
                }
             })
        
        }
</script>