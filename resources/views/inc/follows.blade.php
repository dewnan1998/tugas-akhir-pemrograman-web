<div role="tabpanel" class="tab-pane fade" id="followings">
        
        @if(count($user->follows)>0)
             <h6 class="mb-4" style="opacity: 0.8">{{$user->name}} is follows </h6>
                @foreach ($user->follows as $following )
                    <div class="row border p-2 mb-3"> 
                    <div class="col-2">
                        <img class=" " src="
                        @if( empty($following->avatar))
                            https://via.placeholder.com/150/68ba6d/FFFFFF/?text={{$following->name[0]}}
                        @else
                                /storage/user-avatars/{{$following->avatar}}
                        @endif
                             " alt="" style="height :70px;width: 70px;;wborder-radius: 70px" class="ml-2">
                    </div>
                      
                    <div class="col-10">
                         <a href="/{{'@'.$following->username}}">
                            <h6 class="">
                                    {{$following->name}}
                                </h6>
                            </a>
                                
                            <p>{{$following->short_bio}}</p>
                              
                            @if(Auth::check())
                                @if(!Auth::user()->isMe($following))
                                    <div  class="d-inline-flex mb-4">
                                    @if(!Auth::user()->isFollowing($following->id))
                                        <form id="#followForm"action="" onsubmit="return false"; method='post' >
                                            @csrf
                                            <input id="usernameFollow"type="hidden" name="username" value="{{$following->username}}">
                                            <a 
                                            {{-- href="/{{'@'.$following->username}}/follow" --}} 
                                                onclick="follow('{{$following->username}}')"
                                            class="" style="
                                            color:#32c95f">Follow
                                            </a>
                                        </form>

                                    @else

                                        <form id="#unfollowForm" action="" onsubmit="return false"; method='post' >
                                            @csrf
                                            <input id="usernameUnfollow"type="hidden" name="username" value="{{$following->username}}">

                                            <a onclick="unfollow('{{$following->username}}')" class="" style=" 
                                                color:whit#333;">Following
                                            </a>
                                        
                                        </form>
                            
                                     @endif
                                    </div>
                                 @endif
   
                             @endif    
                    </div>
                </div>        
                @endforeach
            
            @else
            <div class="center align-center mx-auto ">
                    <h4 class="mx-auto" style="opacity: 0.6;">This user is not following anyone</h4>
            </div>
            @endif
       
        
    </div>