@extends('layouts.app2')


@section('content')
<style>
.par
        {
        height: 550px; 

        /* Create the parallax scrolling effect */

        background-attachment: fixed;

        background-repeat: no-repeat;
        background-size: cover;
        z-index: 0;

        }
</style>
<div class="hero-area">
        
        
        
        <div class=" " style="">
            
            <!-- Single Slide -->
            <div class="single-hero-slide bg-img background-overlay par  " style="background-image: url({{asset('img/blog-img/bgd1.jpg')}})  ;"></div>
            <!-- Single Slide -->
        
        </div>

        

        <div class="container shadow-lg pl-5 pr-5 pt-4 pb-4 mr-5 col-lg-4 col-md-6 col-10" style="
            background-color : white;
            position : relative;
            top : -500px;
       
           float: right;

            width : auto;
            border-radius : 10px;
        ">

    

        <h2 style="color: #333"><strong>Sign up</strong></h2>

                <form action="/register" method="post">
                    @csrf
                    <div class="form-group">
                      <label for="">Name</label>
                      <input type="text" name="name" id="" class="form-control" placeholder="" aria-describedby="helpId">
                    </div>
                    
                    <div class="form-group">
                            <label for="">Email</label>
                            <input type="text" name="email" id="" class="form-control" placeholder="" aria-describedby="helpId">
                           
                     </div>
                     
                    <div class="form-group">
                            <label for="">Username</label>
                            <input type="text" name="username" id="" class="form-control" placeholder="" aria-describedby="helpId">
                           
                          </div>

                          
                    <div class="form-group">
                            <label for="">Password</label>
                            <input type="password" name="password" id="" class="form-control" placeholder="" aria-describedby="helpId">
                           
                          </div>

                          <button class="btn btn-primary" type="submit">Sign up</button>
                </form>
        </div>

{{--       
        <div class="alert alert-danger mx-auto" style="position: absolute; top: 200px; left: 29%">
            <strong>Danger!</strong> Indicates a dangerous or potentially negative action.
          </div> --}}
        <!-- Hero Post Slide -->
                
    </div>

    <div class="single-hero-slide bg-img background-overlay par  " style="background-image: url({{asset('img/blog-img/bgd1.jpg')}})  ;"></div>
          

    @endsection