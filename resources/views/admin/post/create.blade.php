@extends('layouts.admin')

@section('style')
<link rel="stylesheet" type="text/css" href="{{asset('css/admin-create-post.css')}}">
@endsection


@section('content')


   

	<form enctype="multipart/form-data" method="post" action="{{action('PostController@store') }}">
    
    {{ csrf_field() }}

    <div class="form-group upload-image">
    	
    		
    	

        <label for="imageInput">
		Thumbnail
        	<div class="thumbnail">
        	<img id="blah" src="http://iconsetc.com/icons-watermarks/flat-circle-white-on-silver/broccolidry/broccolidry_camera/broccolidry_camera_flat-circle-white-on-silver_512x512.png" alt="your image" 
    		style="width: auto; height: 250px; max-width: 100%"  >

    		<div class="overlay"></div>
    		<div class="caption btn btn-light"> <h6>Upload Image </h6></div>
   		</div>

    	</label>


        <input data-preview="#preview" name="thumbnail" type="file" id="imageInput" onchange="readURL(this)">


    </div>

<div class="form-group">
	<label for="judul">Judul </label>
	<input class="form-control" type="text" name="judul" id="judul">

</div>

<div class="form-group">
<label for="editor" >Deskripsi</label>
  <textarea name="content" id="article-ckeditor">
  		
</textarea>
</div>


    <div class="form-group">
  		
        <input class="form-control btn btn-primary" type="submit">
    </div>



</form>

<script type="text/javascript">
	function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah')
                    .attr('src', e.target.result)
                  
            };

            reader.readAsDataURL(input.files[0]);
        }
    }


 </script> 
 
{{--  <script src="https://cdn.ckeditor.com/ckeditor5/11.1.1/classic/ckeditor.js"></script>
 --}}
{{--  <script>
     ClassicEditor
        .create( document.querySelector( '#editor' ) )
        .catch( error => {
            console.error( error );
        } );
        
</script>
 --}}
@endsection