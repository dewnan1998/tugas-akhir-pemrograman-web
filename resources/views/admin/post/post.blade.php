@extends('layouts.admin')

@section('style')
<link rel="stylesheet" type="text/css" href="{{asset('css/manage-post.css')}}">
@endsection


@section('content')

<div class="container">
	
<!-- 	<div class="row ">
		<div class="col-3">  </div>
		<div class="col-9"> </div>
	</div> -->

	<h2 style="border-bottom: 1px solid grey; margin-bottom: 30px;color: gray">Manage Post</h2>
{{-- 	@for ($i = 1; $i <5; $i++)


   	<div class="card flex-row flex-wrap w-100" style="margin-bottom: 30px; box-sizing:border-box; margin-right:100px; " >

		<div class="card-header " >

			<img src="//placehold.it/100" alt="">
		</div>
		<div class="card-block" style="padding: 30px;">
			<h4 class="card-title">Judul Post {{$i}}</h4>
			<p class="card-text">Deskripsi  post</p>		
		</div>
		<div class="w-100"></div>
	</div>

	@endfor --}}


	@foreach($posts as $post)

	<a class="w-100" href="/admin/post/{{$post->id}}" >

		<div class="post-item card flex-row flex-wrap w-100 row" style="margin-bottom: 30px; box-sizing:border-box; margin-right:100px; " >

		<div class="card-header col-3" style="padding: 0" >

			@if(empty($post->thumbnail))
			<img src="//placehold.it/100" alt=""  style="width: 200px;height: 150px">
			@else
			
			<img src="/storage/thumbnails/{{$post->thumbnail}}" alt="" style="width: 200px;height: 150px">
			
			@endif
		</div>
		<div class="card-block col-9" style="padding: 30px;">
		<strong>	<h5 class="card-title" style="color: black">{{
			substr($post->judul,0,45)}}
			@if(strlen($post->judul ) >45)
				 {{ ' ...'}}
			@endif
		</h5></strong>
			<p class="card-text">{{
			strip_tags(substr($post->deskripsi,0,170))}}
			@if(strlen($post->deskripsi) >170)
				 {{ ' ...'}}
			@endif</p>

			<small class="align-bottom">Written on {{$post->created_at}} by {{$post->admin->name}}</small>		
		</div>
		<div class="w-100"></div>

	</div>
		</a>


	@endforeach

<div  class="mx-auto text-center"  style="margin-left: 10%;" >
 {{ $posts->links() }}
</div>
 
 <div class="text-center" style="margin-top: 100px">
<a href="/admin/post/create" class="btn btn-primary btn-lg active mx-auto" role="button" aria-pressed="true"><i class="fas fa-plus-circle"></i> Tambah Post</a>
</div>
</div>

	{{-- <script src="https://cdn.ckeditor.com/ckeditor5/11.1.1/classic/ckeditor.js"></script>


  <textarea name="content" id="editor">
    &lt;p&gt;Here goes the initial content of the editor.&lt;/p&gt;
</textarea>

<script>
    ClassicEditor
        .create( document.querySelector( '#editor' ) )
        .catch( error => {
            console.error( error );
        } );
</script>
 --}}

@endsection