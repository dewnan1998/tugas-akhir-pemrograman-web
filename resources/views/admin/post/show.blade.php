@extends('layouts.admin')

@section('style')
<link rel="stylesheet" type="text/css" href="{{asset('css/show-post.css')}}">
@endsection


@section('content')

<div class="container">
	
	@if(empty($post->thumbnail))
	<img class="w-100" src="//placehold.it/300" style="max-width: 720px">
	@else
	<img class="w-100" src="/storage/thumbnails/{{$post->thumbnail}}" style="max-width: 720px">
	@endif
	<h1>{{$post->judul}}</h1>

	<div>{!!$post->deskripsi!!}</div>
		<small  style="opacity: 0.5" class="align-bottom">Written on {{$post->created_at}} by {{$post->admin->name}}</small>	
	
	</div>

	
@endsection