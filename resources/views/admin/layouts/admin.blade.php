<!DOCTYPE html>
<html>
<head>

	<title>
	</title>


	<link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('css/admin/admin-layout.css')}}">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

	@yield('style')

	
</head>
<body>




	<div class="navbar my-navbar box-shadow sticky-top">
		
		<div class="col-2">
			<a href="/">
			<img src="{{asset('img/core-img/dew-world-logo-black.png')}}">
		</a>
		</div>


		
		<div class=" row  float-right d-flex align-items-center col-10">  
			<div class="col-10"> 
				<form  method="get" class="float-right">
					<input type="text" class="form-control" name="serach" placeholder="Search">
					
				</form> 
			</div>
			<div class="user-icon "> 
				
				<div class="dropdown" >
					<a class="  dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="false" aria-expanded="false">
						<i class="fas fa-bell" style="font-size: 30px;"></i>

					</a>

					<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink" style=""  >
						<a class="dropdown-item" href="#">Action</a>
						<a class="dropdown-item" href="#">Another action</a>
						<a class="dropdown-item" href="#">Something else here</a>
					</div>
				</div>
			</div>
			<div class="user-icon "> 
				<div class="dropdown" >
					<a class="  dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="false" aria-expanded="false">
						<img class=""src="{{asset('img/user.png')}}">

					</a>

					
					

					<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">
						
						<strong class="mx-auto">{{Auth::user()->name}}</strong>
						<a class="dropdown-item" href="/logout">
						{{ __('Logout') }}
					</a>

				
				</div>
				
			</div>
		</div>
	</div>


</div>


<div class="nav-left-sidebar my-sidebar">

	<div class="account"> 
		<div class="row">

			<div class="col-5"> </div>

			<div class="col-7"></div>

		</div>


	</div>



	<div class="nav-item">

		<a href="admin/" class="nav-link active"><i class="fas fa-chart-line"></i>Dashboard</a>
		<a href="admin/manage/posts" class="nav-link active"><i class="fas fa-paste"></i> Manage Post</a>
		<a href="admin/manage/users" class="nav-link active"><i class="fas fa-cube"></i> Manage User</a>
		<a href="#" class="nav-link active	"><i class="far fa-file-alt"></i> Manage Web Info</a>
		
	</div>


</div>

<div class="content ">
	
	<div class=" box-shadow container .box-shadow col-11" style="overflow: hidden; background-color: #fcfcfc;e;padding: 8%;">
		{{-- @include('inc.messages') --}}
		
		@yield('content')
		
	</div>
</div>

<div class="footer text-center">

	<h6> Copyright © 2018 Concept. All rights reserved Kaisul Fuqara Dewananda</h6>
</div>

	<script src="{{ asset('js/app.js') }}"></script>
	<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>

	<script>
		CKEDITOR.replace( 'article-ckeditor' );
	</script>


</body>
</html>