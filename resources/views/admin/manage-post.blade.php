@extends('layouts.admin')

@section('style')
<link rel="stylesheet" type="text/css" href="{{asset('css/manage-post.css')}}">
@endsection


@section('content')

<div class="container">
	
<!-- 	<div class="row ">
		<div class="col-3">  </div>
		<div class="col-9"> </div>
	</div> -->

	<h2 style="border-bottom: 1px solid grey; margin-bottom: 30px;color: gray">Manage Post</h2>
{{-- 	@for ($i = 1; $i <5; $i++)


   	<div class="card flex-row flex-wrap w-100" style="margin-bottom: 30px; box-sizing:border-box; margin-right:100px; " >

		<div class="card-header " >

			<img src="//placehold.it/100" alt="">
		</div>
		<div class="card-block" style="padding: 30px;">
			<h4 class="card-title">Judul Post {{$i}}</h4>
			<p class="card-text">Deskripsi  post</p>		
		</div>
		<div class="w-100"></div>
	</div>

	@endfor --}}


	@foreach($posts -> $post)
		<div class="well flex-row flex-wrap w-100" style="margin-bottom: 30px; box-sizing:border-box; margin-right:100px; " >

		<div class="card-header " >

			<img src="//placehold.it/100" alt="">
		</div>
		<div class="card-block" style="padding: 30px;">
			<h4 class="card-title">{{$post->judul}}</h4>
			<p class="card-text">{{$post->deskripsi}}</p>		
		</div>
		<div class="w-100"></div>
	</div>


	@endfor

<nav aria-label="Page navigation example">
  <ul class="pagination justify-content-center">
    <li class="page-item disabled">
      <a class="page-link" href="#" tabindex="-1">Previous</a>
    </li>
    <li class="page-item active"><a class="page-link" href="#">1</a></li>
    <li class="page-item"><a class="page-link" href="#">2</a></li>
    <li class="page-item"><a class="page-link" href="#">3</a></li>
    <li class="page-item">
      <a class="page-link" href="#">Next</a>
    </li>
  </ul>
</nav>
 
 <div class="text-center" style="margin-top: 100px">
<a href="#" class="btn btn-primary btn-lg active mx-auto" role="button" aria-pressed="true">Tambah Post</a>
</div>
</div>


@endsection