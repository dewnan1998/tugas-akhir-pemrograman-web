<!DOCTYPE html>
<html>
<head>
	<title>
		Login Admin
	</title>
	<link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('css/login.css')}}">
</head>
<body>


	<div class="container mx-auto col-lg-4 col-md-6 col-12">
<div class="header text-center" style="margin-bottom: 5%;"> 
			<img class="col-6  col-sm-4 col-md-6" src="{{asset('img/logo.png')}}">
			<h2 class="display-4" style="font-size: 30px;">LOGIN ADMIN</h2>

		</div>
		<div class="form-container box">
				
			<form method="POST" action="{{ route('admin/login') }}" >
				@csrf

				<div class="form-group">
					<label for="username" style="opacity: 0.8;"><strong>Username</strong></label>
					<input type="text" class="form-control" id="username"  placeholder="Masukan Username" name="username">
				 
				</div>
				<div class="form-group">
					<label for="exampleInputPassword1" style="opacity: 0.8;"> <strong>Password</strong></label>
					<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Masukan Password" name="password">
				</div>
				 <div class="form-group">

                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                            <label class="form-check-label" for="remember">
                                {{ __('Remember Me') }}
                            </label>
                        </div>
                    
                 </div>


				<button  style="text-align: center"type="submit" class="btn btn-primary">Log In</button>
		
			</form>
		</div>
		<div class="footer-container">
		
			<span  style="margin-bottom: 20px;"><a href="/"><- Kembali Ke Website SD Muhmmadiyah 2 Surabaya</a></span>
<br><br>
		<h6 class="mx-auto text-cnter"style="opacity: 0.5; text-align: center">
			Copyright &copy SD Muhammadiyah 2 Surabaya	
		</h6>
		</div>

	</div>

</body>
</html>