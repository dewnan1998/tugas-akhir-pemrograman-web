<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="{{asset('js/jquery/jquery-2.2.4.min.js')}}"></script>
    
    
    <style>
            </style>

<link rel="stylesheet" href="{{asset('css/chatbox.css')}}">
    
    <link rel="stylesheet" href="{{asset('css/app.css')}}">


    <link rel="stylesheet"  href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}">
    
	<link rel="stylesheet"  href="{{asset('fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
	<link rel="stylesheet"  href="{{asset('fonts/iconic/css/material-design-iconic-font.min.css')}}">
	<link rel="stylesheet"  href="{{asset('vendor/animate/animate.css')}}">
	<link rel="stylesheet"  href="{{asset('vendor/css-hamburgers/hamburgers.min.css')}}">
	<link rel="stylesheet"  href="{{asset('vendor/animsition/css/animsition.min.css')}}">
	<link rel="stylesheet"  href="{{asset('vendor/select2/select2.min.css')}}">
	<link rel="stylesheet"  href="{{asset('vendor/daterangepicker/daterangepicker.css')}}">
	<link rel="stylesheet"  href="{{asset('css/util.css')}}">
	{{-- <link rel="stylesheet"  href="{{asset('css/login.css')}}"> --}}

    <link rel="icon" href="{{asset('img/core-img/d-logo2.png')}}">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <link rel="stylesheet" href="{{asset('css/font-collection.css')}}">
    <!-- Title  -->
    <title>Dew World</title>

    <!-- Favicon  -->
    <link rel="icon" href="img/core-img/favicon.ico">

    <!-- Style CSS -->
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <style>
        /* .dropdown {text-align:center;}
.button, .dropdown-menu {margin:10px auto}
.dropdown-menu {width:auto; left:50%; margin-left:-100px;} */
.dropdown-toggle::after {
    display:none;
}
    </style>

@yield('styles')
</head>

<body>
    <!-- Preloader Start -->
    {{-- <div id="preloader">
        <div class="preload-content">
            <div id="world-load"></div>
        </div>
    </div> --}}
    <!-- Preloader End -->

    <!-- ***** Header Area Start ***** -->
    
    <header class="header-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <nav class="navbar navbar-expand-lg">
                        <!-- Logo -->
                        <a class="navbar-brand" href="/"><img src="{{asset('img/core-img/dew-world-logo-white.png')}}" alt="Logo" style="max-width : 150px;"></a>
                        <!-- Navbar Toggler -->
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#worldNav" aria-controls="worldNav" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                        <!-- Navbar -->
                        <div class="collapse navbar-collapse" id="worldNav">
                            <ul class="navbar-nav ml-auto">
                                
                                <li class="nav-item mr-4 mt-1">
                                        <div id="search-wrapper">
                                                <form action="#">
                                                    <input type="text" id="search" placeholder="Search something...">
                                                    <div id="close-icon"></div>
                                                    <input class="d-none" type="submit" value="">
                                                </form>
                                            </div>
                                </li>

                                @if(!Auth::check())
                          
                                <li class="nav-item">
                                    <a class="nav-link sign-button" href="#" data-toggle="modal" data-target="#login-modal" style="">SIGN IN</a>
                                </li>
                                @else
                                
                                
                                   
   
                                    <li class="nav-item mr-4 mt-2">
                                        <div class="dropdown open">
                                            <a class=" dropdown-toggle" type="" id="triggerId2" data-toggle="dropdown" aria-haspopup="false"
                                                    aria-expanded="false">
                                                  
                                                 <i class="fa fa-bell" aria-hidden="true" style="color:white;font-size: 20px"></i>
                                                 <small class="p-1"style="background: green; color: white; border-radius:70px; ">{{count(Auth::user()->unreadNotifications)}}</small>
                                                </a>
                                            <div class="dropdown-menu dropdown-menu-right mt-2 p-1" aria-labelledby="triggerId2" style=" height : 300px; overflow-y: scroll;overflow-x: scroll">
                                               
                                                <div class="container" style="border-bottom: gray 1px solid">
                                                    <strong>Notificaiton</strong>
                                                </div>
                                                @foreach (Auth::user()->unreadNotifications as $notification)
                                                
                                                
                                                <a class="dropdown-item mt-2 p-4 vertical-center" href="
                                                @if(!empty($notification->data['links']))
                                                        {{$notification->data['links']}}
                                                @endif">
                                                    <div class="row">
                                                            @if(!empty($notification->data['img_src']))
                                                        <div class="col-3">
                                                           
                                                          <img src="{{$notification->data['img_src']}}" alt=""
                                                            style="object-fit: cover; height: 55;
                                                            border-radius: 70px;"
                                                          >

                                                        </div>
                                                        @endif
                                                        <div class="col-9 vertical-center justify-content-center" style="overflow-wrap: break-word ">
                                                      {!!$notification->data['message']!!}
                                                        </div>
                                                    </div>
                                                   </a>


                                            @endforeach
                                            </div>
                                        </div>
                                      
                                    </li>



                                <li class="nav-item ">
                                    <div class="dropdown open">
                                        <a class=" dropdown-toggle" type="" id="triggerId" data-toggle="dropdown" aria-haspopup="true"
                                                aria-expanded="false">
                                            
                                                <img class=" " src="
                                                @if( empty(Auth::user()->avatar))
                                                https://via.placeholder.com/150/68ba6d/FFFFFF/?text={{Auth::user()->name[0]}}
                                                @else
                                                    {{-- /storage/user-avatars/{{Auth::user()->avatar}} --}}
                                                    {{Auth::user()->avatar}}
                                                @endif
                                               " alt="" style="width :40px; height: 40px;;border-radius: 70px;object-fit: cover" class="ml-5">
                                                
                                            </a>
                                        <div class="dropdown-menu mt-2 p-1 dropdown-menu-right" aria-labelle    dby="triggerId" style="width:200px;">
                                            <a class="dropdown-item mt-2" href="/write  ">Write story</a>
                                            <a class="dropdown-item mt-2" href="/me/stories">Stories</a>
                                            <a class="dropdown-item mt-2" href="/me/stats">Stats</a>
                                            <div class="mt-2" style="border-bottom: 1px solid gray; opacity: 0.1"> </div>
                                        <a class="dropdown-item mt-2" href="/{{'@'.Auth::user()->username}}">Profile</a>
                                            <a class="dropdown-item mt-2" href="/me/settings">Setting</a>
                                            <div class="mt-2" style="border-bottom: 1px solid gray; opacity: 0.1"> </div>
                                            <a class="dropdown-item text-primary" href="/logout">Logout</a>
                                        </div>
                                    </div>
                                  
                                </li>
                                @endif
                            </ul>
                            <!-- Search Form  -->
                          


                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- ***** Header Area End ***** -->

        @yield('content')
    
   

        {{-- @include('inc.chatbox') --}}

    @if(!Auth::check())
        @include('inc.modal-login')
        @include('inc.modal-register')
    @endif


   
 
      <!-- Popper js -->
      <script src="{{asset('js/popper.min.js')}}"></script>
      <!-- Bootstrap js -->
      <script src="{{asset('js/bootstrap.min.js')}}"></script>
      <!-- Plugins js -->
      <script src="{{asset('js/plugins.js')}}"></script>
      <!-- Active js -->
      <script src="{{asset('js/active.js')}}"></script>
     
    <script src="{{asset('js/login.js')}}"></script>
    
    
  
   
</body>

</html>