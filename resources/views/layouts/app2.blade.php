<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="{{asset('js/jquery/jquery-2.2.4.min.js')}}"></script>
    
    @yield('styles')

    <link rel="stylesheet" href="{{asset('css/app.css')}}">


	<link rel="stylesheet"  href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}">
	<link rel="stylesheet"  href="{{asset('fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
	<link rel="stylesheet"  href="{{asset('fonts/iconic/css/material-design-iconic-font.min.css')}}">
	<link rel="stylesheet"  href="{{asset('vendor/animate/animate.css')}}">
	<link rel="stylesheet"  href="{{asset('vendor/css-hamburgers/hamburgers.min.css')}}">
	<link rel="stylesheet"  href="{{asset('vendor/animsition/css/animsition.min.css')}}">
	<link rel="stylesheet"  href="{{asset('vendor/select2/select2.min.css')}}">
	<link rel="stylesheet"  href="{{asset('vendor/daterangepicker/daterangepicker.css')}}">
	<link rel="stylesheet"  href="{{asset('css/util.css')}}">
	{{-- <link rel="stylesheet"  href="{{asset('css/login.css')}}"> --}}

    <link rel="icon" href="{{asset('img/core-img/d-logo2.png')}}">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <link rel="stylesheet" href="{{asset('css/font-collection.css')}}">
    <!-- Title  -->
    <title>Dew World</title>

    <!-- Favicon  -->
    <link rel="icon" href="img/core-img/favicon.ico">

    <!-- Style CSS -->
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>

<body>
    <!-- Preloader Start -->
    {{-- <div id="preloader">
        <div class="preload-content">
            <div id="world-load"></div>
        </div>
    </div> --}}
    <!-- Preloader End -->

    <!-- ***** Header Area Start ***** -->
    
    <!-- ***** Header Area End ***** -->

        @yield('content')
    
        
    <!-- ***** Footer Area Start ***** -->

      <!-- jQuery (Necessary for All JavaScript Plugins) -->
 
      <!-- Popper js -->
      <script src="{{asset('js/popper.min.js')}}"></script>
      <!-- Bootstrap js -->
      <script src="{{asset('js/bootstrap.min.js')}}"></script>
      <!-- Plugins js -->
      <script src="{{asset('js/plugins.js')}}"></script>
      <!-- Active js -->
      <script src="{{asset('js/active.js')}}"></script>
     
    <script src="{{asset('js/login.js')}}"></script>
    
  
   
</body>

</html>