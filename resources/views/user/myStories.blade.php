@extends('layouts.app')

@section('content')

<div class="container">

        {{-- enter spacing --}}
        <div class="row mt-5 w-100"></div>

        <div class="container mt-5  mb-5 "> 
                @include('inc.messages')
                <div class="pb-3 mt-5 mb-3 flex "style="border-bottom: 1px grey solid;  overflow: hidden ">

                    <h2 class="font-weight-bold inline">Your stories</h2>

                    <a href="/write" class=""  style="padding :5px;;border-radius: 5px; float : right; ;border: 1px green solid; padding 2px; color: green !important;">Write Story</a>
                    <a href="#" class="mr-2"  style="padding :5px;border-radius: 5px; float : right; ;border: 1px gray solid; padding 2px; color: gray !important;">Import Story</a>
                  
                </div>
        @if(count($posts)>0)
        @foreach($posts as $post)

       
    
        <div class=" w-100 row" style="margin-bottom: 20px;">

        <div class="card-header col-md-4 col-sm-12 col-lg-2" style="padding: 0" >

            @if(empty($post->thumbnail))
            <img src="//placehold.it/100" alt=""  style="object-fit: cover; width 100%;height: 170px">
            @else
            
            <img src="/storage/thumbnails/{{$post->thumbnail}}" alt="" style="object-fit: cover; width 170px;height: 170px">
            
            @endif  
        </div>
        <div class="card-block col" style="padding: 30px;">
        
            <a class="w-100" href="/p/{{$post->meta}}"  style="border-bottom: 1px gray solid"><strong>	<h3 class="" style="color: black opacity: 0.8;font-family: KievitBold !important">{{
            substr($post->title,0,45)}}
            @if(strlen($post->title ) >45)
                 {{ ' ...'}}
            @endif
        </h3>
    </strong></a>
            <p class="card-text">{{
            strip_tags(substr($post->description,0,170))}}
            @if(strlen($post->description) >170)
                 {{ ' ...'}}
            @endif</p>

            <small class="align-bottom">Written on {{$post->created_at}} by {{$post->user->name}}
            
                
                <div class="btn-group">
                    <button style="background: none; color: gray;border: none;"class="btn btn-secondary btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                       <i class="fa fa-caret-down" aria-hidden="true"></i>
                    </button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="/p/{{$post->meta}}/edit">Edit Story</a>
                            <form method="POST" action="/p/{{$post->meta}}/delete">
                                 @csrf
                                <a > 
                                    <button  type="submit" class="dropdown-item" href="#">Delete Story</button>
                                </a>
                            </form>
                                <a class="dropdown-item" href="/me/stats/post/{{$post->meta}}">View Stats</a>
             
                        </div>
                  </div>

            </small>		
        </div>
        <div class="w-100"></div>

    </div>
        


    @endforeach
    
<div  class="mx-auto text-center mb-5"  style="margin-left: 10%;" >
 {{ $posts->links() }}
</div>
     
@else
<div class="container text-center m-5">
        <h1 class="mx-auto" style="opacity: 0.4; vertical-align:middle"> You Dont have stories</h1>
        
        <a href="/write">Add one</a>
</div>
    @endif
        
     
     {{-- <div class="text-center" style="margin-top: 100px">
    <a href="/admin/post/create" class="btn btn-primary btn-lg active mx-auto" role="button" aria-pressed="true"><i class="fas fa-plus-circle"></i> Tambah Post</a>
    </div>     --}}
</div>  
</div>  

@endsection