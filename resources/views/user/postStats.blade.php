@extends('layouts.app')

@section('content')

<div class="container">

        {{-- enter spacing --}}
        <div class="row mt-5 w-100"></div>

        <div class="container mt-5  mb-5 "> 
                @include('inc.messages')
                <div class="pb-3 mt-5 mb-3 flex pt-5 border-bottom"style="  overflow: hidden ">

    <a href="#" class="mr-2"  style="padding :5px;border-radius: 5px; float : right; ;border: 1px gray solid; padding 2px; color: gray !important;">See all Stats</a>

                    <p style="font-size: 16px">Published on {{$post->created_at->format('m j, Y')}} </p>
                    <h1 class="font-weight-bold inline">{{$post->title}}</h1>
                    <p class="font-bold">TOTAL VIEWS</p>
                     <h1 class="font-weight-bold inline">{{$post->views}}</h1>

                  
                </div>
        
     
     {{-- <div class="text-center" style="margin-top: 100px">
    <a href="/admin/post/create" class="btn btn-primary btn-lg active mx-auto" role="button" aria-pressed="true"><i class="fas fa-plus-circle"></i> Tambah Post</a>
    </div>     --}}
</div>  
</div>  

@endsection