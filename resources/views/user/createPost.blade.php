@extends('layouts.app')

@section('styles')
<link rel="stylesheet" type="text/css" href="/post/store">
<script src="https://cdn.jsdelivr.net/npm/medium-editor@latest/dist/js/medium-editor.min.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/medium-editor@latest/dist/css/medium-editor.min.css" type="text/css" media="screen" charset="utf-8">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <!-- Style CSS -->
<style type="text/css">
            
    .label-image
    {
        position: relative; bottom: 100px; color: white

    }
    .label-image:hover
    {
        cursor: pointer;
    }
</style>



@endsection


@section('content')


    {{-- <div style="height : 200px;"> </div>
   <div class="container pt-5" style="">

	<form enctype="multipart/form-data" method="post" action="{{action('UserActivityController@addPost') }}">
    
    {{ csrf_field() }}

    <div class="form-group upload-image">
    	
    		
    	

        <label for="imageInput">
		Thumbnail
        	<div class="thumbnail">
        	<img id="blah" src="http://iconsetc.com/icons-watermarks/flat-circle-white-on-silver/broccolidry/broccolidry_camera/broccolidry_camera_flat-circle-white-on-silver_512x512.png" alt="your image" 
    		style="width: auto; height: 250px; max-width: 100%"  >

    		<div class="overlay"></div>
    		<div class="caption btn btn-light"> <h6>Upload Image </h6></div>
   		</div>

    	</label>


        <input data-preview="#preview" name="thumbnail" type="file" id="imageInput" onchange="readURL(this)">


    </div>

<div class="form-group">
	<label for="judul">Judul </label>
	<input class="form-control" type="text" name="judul" id="judul">

</div>

<div class="form-group">
<label for="editor" >Deskripsi</label>
  <textarea name="content" id="article-ckeditor">
  		
</textarea>
</div>


    <div class="form-group">
  		
        <input class="form-control btn btn-primary" type="submit">
    </div>



</form>
   </div>
<script type="text/javascript">
	function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah')
                    .attr('src', e.target.result)
                  
            };

            reader.readAsDataURL(input.files[0]);
        }
    }


 </script> 
 
 <script src="https://cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>

 <script>
     
     CKEDITOR.replace( 'content' );
        
</script> --}}

<div id="bg"class="hero-area height-600 bg-img background-overlay" style="background-image: url(img/blog-img/bg2.jpg);">
    <div class="container h-100">
        <div class="row h-100 align-items-center justify-content-center">
            <div class="col-12 col-md-8 col-lg-6">
                <div class="single-blog-title text-center">
                    <!-- Catagory -->
                   
                    <h3 id="title"class="editable-tittle"></h3>
                </div>
            </div>
        
        </div>
        <div class="form-group upload-image">
    
        
       
        
<form  enctype="multipart/form-data" id="form" method="post" action="{{action('UserActivityController@addPost') }}" >
    @csrf

    <input id="titleInput" type="text" name="judul" style="display: none">
    <textarea id="contentInput" name="content" style="display: none"></textarea>
    
    <label class="label-image"for="imageInput" style=""> Upload Thumbnail</label>

        <input data-preview="#preview" name="thumbnail" type="file" id="imageInput" onchange="readURL(this)"
            style="display: none"
        >


</form>

@include('inc.messages')
 

        </div>
    </div>

</div>
<script>
    var editor = new MediumEditor('.editable-tittle', {
    keyboardCommands: false,    
    toolbar: false,
    disableReturn:true,
    maxLength : 20,

    placeholder: {
        text: 'Title '
    }
    });
</script>
<!-- ********** Hero Area End ********** -->

<div class="main-content-wrapper section-padding-100">
    <div class="container">
        <div class="row justify-content-center">
            <!-- ============= Post Content Area ============= -->
            <div class="col-12 col-lg-8">
                <div class="single-blog-content mb-100">
                    <!-- Post Meta -->
                  
                    <!-- Post Content -->


                   <style type="text/css">
                       
                            .editor *{
                                        color: black;

                            }
                            .editor p
                            {
                                font-size: 20px;
                            }

                   </style>
                   
                   <div class="editable medium-editor-insert-plugin" data-placeholder="Type some text" contenteditable="true" spellcheck="true" data-medium-editor-element="true" role="textbox" aria-multiline="true" medium-editor-index="0" data-medium-focused="true">
                    <h2><br></h2>
        
                    <p class="medium-insert-active"><br></p>
        
                    <p>And we also support embedding of videdadaos, social media, etc.</p>
        
                    <div class="medium-insert-embeds" contenteditable="false">
                        <figure>
                            <div class="medium-insert-embed">
                                <div><div style="left: 0px; width: 100%; height: 0px; position: relative; padding-bottom: 56.2493%;"><iframe src="https://www.youtube.com/embed/_rDtZgLolHQ?wmode=transparent&amp;rel=0&amp;autohide=1&amp;showinfo=0&amp;enablejsapi=1" frameborder="0" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true" style="top: 0px; left: 0px; width: 100%; height: 100%; position: absolute;"></iframe></div></div>
                            </div>
                            <figcaption contenteditable="true">Casey Neistat, who else...</figcaption>
                        </figure>
                    <div class="medium-insert-embeds-overlay"></div></div>
        
                    <p>Have fun.</p>
        
                    <h4>What to do next?</h4>
        
                    <p>Please, go to <a href="https://github.com/orthes/medium-editor-insert-plugin">our GitHub repository</a> for installation and configuration info. Did you find a bug, have an idea or want to contribute? Great! Do not miss our <a href="https://github.com/orthes/medium-editor-insert-plugin/issues">issues page on GitHub</a>.</p>
        
                    <h4>Contributors</h4>
        
                    <p>This plugin is brought to you by these great contributors from all around the world:</p>
        
                    <div class="medium-insert-images medium-insert-images-grid small-grid">
                        <figure contenteditable="false"><img src="uploads/312938.jpg" alt=""></figure>
                        <figure contenteditable="false"><img src="uploads/62333.png" alt=""></figure>
                        <figure contenteditable="false"><img src="uploads/39333.jpg" alt=""></figure>
                        <figure contenteditable="false"><img src="uploads/19343.jpg" alt=""></figure>
                        <figure contenteditable="false"><img src="uploads/79373.jpg" alt=""></figure>
                        <figure contenteditable="false"><img src="uploads/1228229.jpg" alt=""></figure>
                        <figure contenteditable="false"><img src="uploads/594298.jpg" alt=""></figure>
                        <figure contenteditable="false"><img src="uploads/5272569.jpg" alt=""></figure>
                        <figure contenteditable="false"><img src="uploads/1790778.jpg" alt=""></figure>
                        <figure contenteditable="false"><img src="uploads/4083642.jpg" alt=""></figure>
                        <figure contenteditable="false"><img src="uploads/1101183.jpg" alt=""></figure>
                        <figure contenteditable="false"><img src="uploads/431361.jpg" alt=""></figure>
                        <figure contenteditable="false"><img src="uploads/1086365.jpg" alt=""></figure>
                        <figure contenteditable="false"><img src="uploads/93555.png" alt=""></figure>
                        <figure contenteditable="false"><img src="uploads/883073.jpg" alt=""></figure>
                        <figure contenteditable="false"><img src="uploads/1891369.jpg" alt=""></figure>
                        <figure contenteditable="false"><img src="uploads/1642706.jpg" alt=""></figure>
                        <figure contenteditable="false"><img src="uploads/1270102.jpg" alt=""></figure>
                        <figure contenteditable="false"><img src="uploads/28541.jpg" alt=""></figure>
                        <figure contenteditable="false"><img src="uploads/610268.png" alt=""></figure>
                        <figure contenteditable="false"><img src="uploads/525103.jpg" alt=""></figure>
                        <figure contenteditable="false"><img src="uploads/1831399.jpg" alt=""></figure>
                        <figure contenteditable="false"><img src="uploads/1485056.jpg" alt=""></figure>
                        <figure contenteditable="false"><img src="uploads/55442.png" alt=""></figure>
                        <figure contenteditable="false"><img src="uploads/1064889.jpg" alt=""></figure>
                        <figure contenteditable="false"><img src="uploads/714146.jpg" alt=""></figure>
                        <figure contenteditable="false"><img src="uploads/882228.jpg" alt=""></figure>
                        <figure contenteditable="false"><img src="uploads/433501.jpg" alt=""></figure>
                        <figure contenteditable="false"><img src="uploads/1912864.png" alt=""></figure>
                        <figure contenteditable="false"><img src="uploads/28841.jpg" alt=""></figure>
                        <figure contenteditable="false"><img src="uploads/366564.jpg" alt=""></figure>
                        <figure contenteditable="false"><img src="uploads/6654724.jpg" alt=""></figure>
                    </div>
        
                    <p><em>... And yes, you can create this nice images grid with the plugin. And much more...</em></p>
        
                    <h4>License</h4>
        
                    <p>The plugin is free and released under MIT license.</p>
                <div class="medium-insert-buttons" contenteditable="false" style="left: 15px; top: 173.104px; display: block;">
            <a class="medium-insert-buttons-show">+</a>
            <ul class="medium-insert-buttons-addons" style="display: none;">
                    <li style="display: inline-block;"><a data-addon="images" data-action="add" class="medium-insert-action"><span class="fa fa-camera"></span></a></li>
                    <li style="display: inline-block;"><a data-addon="embeds" data-action="add" class="medium-insert-action"><span class="fa fa-youtube-play"></span></a></li>
            </ul>
        </div></div> <div id="content" class="post-content editor" style="color: black">
                       
                        <!-- Post Tags -->
                    
                        <!-- Post Meta -->
                      
                    </div>
                </div>
                <div class="container float-right" style="opacity: 0.6">
               

                <a id="save"> <h4 class="float-right" style="border: 1px solid black; border-radius: 20px; padding: 8px;">
                   <i class="fas fa-plus-circle"></i> Publish
                </h4>
                </a>

                <a href=""> <h4 class="float-right mr-3" style="border: 1px solid #7397d1; border-radius: 20px; padding: 8px;color: #7397d1">
                    <i class="fas fa-save"></i> Draft
                </h4>
                </a>
                </div>
            </div>

        </div>

    </div>

</div>


<script type="text/javascript">

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#bg')
                .css("background-image", 'url('+e.target.result+')')


                     console.log("url : "+e.target.result);  
             };



        reader.readAsDataURL(input.files[0]);
    }
}


var editor = new MediumEditor('.editable'
            );

        $(function () {
            $('.editable').mediumInsert({
                editor: editor,
                addons: {
                    images: {
                        uploadScript: null,
                        deleteScript: null,
                        captionPlaceholder: 'Type caption for image',
                        styles: {
                            slideshow: {
                                label: '<span class="fa fa-play"></span>',
                                added: function ($el) {
                                    $el
                                        .data('cycle-center-vert', true)
                                        .cycle({
                                            slides: 'figure'
                                        });
                                },
                                removed: function ($el) {
                                    $el.cycle('destroy');
                                }
                            }
                        },
                        actions: null
                    }
                }
            });
        });

        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-44692164-1', 'auto');
        ga('send', 'pageview');
    
 
$('#save').click(function () {
    var title = $('#title').html();
    var content = $('#content').html();

    $('#titleInput').val(title);
    $('#contentInput').val(content);

    $("#form").submit();
    // $('#hiddeninput').append(mysave);
    // alert($('#hiddeninput').val());
});

 

</script>

@endsection