@extends('layouts.app')

@section('content')

<div class="container">

        {{-- enter spacing --}}
        <div class="row mt-5 w-100"></div>

        <div class="container mt-5  mb-5 "> 
                @include('inc.messages')
                <div class="pb-3 mt-5 mb-3 flex pt-5 border-bottom"style="  overflow: hidden ">

    <a href="#" class="mr-2"  style="padding :5px;border-radius: 5px; float : right; ;border: 1px gray solid; padding 2px; color: gray !important;">See all Stats</a>

                    <p style="font-size: 16px">Member Since {{$user->created_at->format('m j, Y')}} </p>
                    <h1 class="font-weight-bold inline">Stats</h1>
                    <p class="font-bold">Click story below to view in chart</p>
                    <div class="row mb-5 border-top">
                      <div class="col-4 border-right">
                            <h1 class="font-weight-bold inline mb-0">{{count($user->followers)}}</h1>
                            <h4 class="mt-0" style="font-family: KievitBold">Fans</h4>
                     </div>
                    <div class="col-4 border-right">
                        <h1 class="font-weight-bold inline mb-0">{{count($user->posts)}}</h1>
                        <h4 class="mt-0" style="font-family: KievitBold">Posts</h4>        
                    </div>
                    <div class="col-4">
                            <h1 class="font-weight-bold inline mb-0">   {{$user->posts->sum('views')}}</h1>
                            <h4 class="mt-0" style="font-family: KievitBold">Views</h4>
                      </div>
                    </div>
                     

                     <div class="col-12 col-md-offset-1">
                        <div class="panel panel-default">
                            <div class="panel-heading"><b>Charts</b></div>
                            <div class="panel-body">
                                <canvas id="canvas" height="280" width="600"></canvas>
                            </div>
                        </div>
                    </div>
                </div>

     {{-- <div class="text-center" style="margin-top: 100px">
    <a href="/admin/post/create" class="btn btn-primary btn-lg active mx-auto" role="button" aria-pressed="true"><i class="fas fa-plus-circle"></i> Tambah Post</a>
    </div>     --}}
</div>  
</div>  


<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.3/js/bootstrap-select.min.js" charset="utf-8"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.bundle.js" charset="utf-8"></script>
<script>

var dates = new Array();
// var Labels = new Array();
var count = new Array();
$(document).ready(function(){

    var followers = JSON.parse('{!! json_encode($followers) !!}');
    for (i in followers)
        {
            console.log(followers);
            dates.push(followers[i].date);
            count.push(followers[i].jumlah)
        }
    

    var ctx = document.getElementById("canvas").getContext('2d');
        var myChart = new Chart(ctx, {
          type: 'bar',
          
          data: {
              labels:dates,
              datasets: [{
                  label: 'Total Follower',
                  data: count,
                  borderWidth: 1
              }],
              borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
          },

          options: {
              scales: {
                  yAxes: [{
                      ticks: {
                          beginAtZero:true
                      }
                  }]
              }
          }
      });
  });

</script>
@endsection