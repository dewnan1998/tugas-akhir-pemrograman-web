@extends('layouts.app')

@section('styles')



<script src="https://cdn.jsdelivr.net/npm/medium-editor@latest/dist/js/medium-editor.min.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/medium-editor@latest/dist/css/medium-editor.min.css" type="text/css" media="screen" charset="utf-8">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

<style>
a{
    cursor: pointer;
 }
 
 .medium-editor-placeholder:after {
    text-align: center;
    width:100%;
    opacity: 0.6;
    }
    
</style>

@endsection


@section('content')
<div id="follow_div_author" >

    @if(empty($user->cover_image))
        <div class="hero-area height-400 bg-img background-overlay" style="background-image: url(http://thealphagroup.net.au/wp-content/uploads/2015/04/blue-background.jpg);">
    @else
        <div class="hero-area height-400 bg-img background-overlay" style="background-image: url(/storage/user-cover-images/{{$user->cover_image}});">
    @endif
    <div class="container h-100">
        <div class="row h-100 align-items-center justify-content-center">
            <div class="col-12 col-md-8 col-lg-6">
                <div class="single-blog-title text-center">
                    <!-- Catagory -->
                 </div> 
            </div>
        </div>
    </div>
    <div>
    
</div>
</div> 


<div id="" class="container col-lg-4 col-md-8 col-sm-12 text-center " style="position: relative; bottom: 70px; z-index: 100">
    <div>
    <img id="avatar" class=" " src="
    @if( empty($user->avatar))
    https://via.placeholder.com/150/68ba6d/FFFFFF/?text={{$user->name[0]}}
    @else
        {{-- /storage/user-avatars/{{$user->avatar}} --}}
        {{$user->avatar}}
    
    @endif
   " alt="" style="width :130px;height: 130px; object-fit: cover; border-radius: 70px" class="ml-5">
</div>
    
    <h3 id="editName" class="editableName" class="mt-3" style="font-family:KievitBold; opacity: 0.8;  ">{{$user->name}}</h3>
    <p  id="editBio" class="editableBio">{{$user->short_bio}} </p>
    
    
    <div class="text-left d-inline-flex mt-4 mb-5   ">
            <a id="save" class="ml-2 p-2 pl-3 pr-3"  style="color: green; border: 1px solid green">Save</a>    
            <a  href="/{{'@'.$user->username}}"class="ml-2 border p-2 pl-3 pr-3" href="color: #ccc; border-color:#ccc">Cancel</a>    
        </div>    
</div>


</div>


<form style="" enctype="multipart/form-data" id="form" method="post" action="/{{'@'.$user->username}}/update"  >
    @csrf
    <input type="hidden" name="username" value="{{$user->username}}">
    <input id="inputName" type="hidden" name="name" >
    <input type="hidden" id="inputBio" name="short_bio">
    
    <label class="label-image text-center" for="avatar-input" style=" display:block;z-index: 300;" > 
        <img id="image-label" src="https://cdn0.iconfinder.com/data/icons/basic-outline/64/icon-basic-set_12-camera-512.png" style=" z-index: 320; color: black; position:relative ; bottom: 400px;
       padding: 5px; width: 130px; height: 130px; background: gray;opacity: 0.4;;border-radius: 70px;">
    </label>
        
    <input  onchange="changeAvatar(this)"data-preview="#preview"type="file" name="avatar" id="avatar-input" style="display:none">

        <label class="d- label-image float-right"for="imageInput" style=""> <h6 style=" z-index: 400;color: white; position: relative; bottom : 500px;; margin-right: 30px;"><i class="fas fa-images"></i>Upload Cover Image <h6></label>

        <input data-preview="#preview" name="cover_image" type="file" id="imageInput" onchange="changeCover(this)"
            style="display: none">
</form>

<script>

function changeCover(input) {
            if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#bg')
                    .css("background-image", 'url('+e.target.result+')')


                        console.log("url : "+e.target.result);  
                };



            reader.readAsDataURL(input.files[0]);
        }

}   
    function changeAvatar(input) {
        if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#avatar')
            .attr('src', e.target.result)


                     console.log("url : "+e.target.result);  
             };



        reader.readAsDataURL(input.files[0]);
    }
}
</script>
<script>
        var editor = new MediumEditor('.editableName', {
        keyboardCommands: false,    
        toolbar: false,
        disableReturn:true,
        maxLength : 20,
    
        placeholder: {
            text: 'Enter Your Name '
        }
        });
        var editor2 = new MediumEditor('.editableBio', {
        keyboardCommands: false,    
        toolbar: false,
        disableReturn:true,
        maxLength : 20,
    
        placeholder: {
            text: 'Enter a Short Bio '
        }
        });

$('#save').click(function () {
    var name = $('#editName').html();
    var shortBio = $('#editBio').html();

    $('#inputName').val(name);
    $('#inputBio').val(shortBio);

    $("#form").submit();
    // $('#hiddeninput').append(mysave);
    // alert($('#hiddeninput').val());
});

    </script>

@endsection