@extends('layouts.app')

@section('styles')
<link rel="stylesheet" type="text/css" href="/post/store">
<script src="https://cdn.jsdelivr.net/npm/medium-editor@latest/dist/js/medium-editor.min.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/medium-editor@latest/dist/css/medium-editor.min.css" type="text/css" media="screen" charset="utf-8">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <!-- Style CSS -->
<style type="text/css">
            
    .label-image
    {
        position: relative; bottom: 100px; color: white

    }
    .label-image:hover
    {
        cursor: pointer;
    }
</style>



@endsection


@section('content')


    {{-- <div style="height : 200px;"> </div>
   <div class="container pt-5" style="">

	<form enctype="multipart/form-data" method="post" action="{{action('UserActivityController@addPost') }}">
    
    {{ csrf_field() }}

    <div class="form-group upload-image">
    	
    		
    	

        <label for="imageInput">
		Thumbnail
        	<div class="thumbnail">
        	<img id="blah" src="http://iconsetc.com/icons-watermarks/flat-circle-white-on-silver/broccolidry/broccolidry_camera/broccolidry_camera_flat-circle-white-on-silver_512x512.png" alt="your image" 
    		style="width: auto; height: 250px; max-width: 100%"  >

    		<div class="overlay"></div>
    		<div class="caption btn btn-light"> <h6>Upload Image </h6></div>
   		</div>

    	</label>


        <input data-preview="#preview" name="thumbnail" type="file" id="imageInput" onchange="readURL(this)">


    </div>

<div class="form-group">
	<label for="judul">Judul </label>
	<input class="form-control" type="text" name="judul" id="judul">

</div>

<div class="form-group">
<label for="editor" >Deskripsi</label>
  <textarea name="content" id="article-ckeditor">
  		
</textarea>
</div>


    <div class="form-group">
  		
        <input class="form-control btn btn-primary" type="submit">
    </div>



</form>
   </div>
<script type="text/javascript">
	function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah')
                    .attr('src', e.target.result)
                  
            };

            reader.readAsDataURL(input.files[0]);
        }
    }


 </script> 
 
 <script src="https://cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>

 <script>
     
     CKEDITOR.replace( 'content' );
        
</script> --}}

<div id="bg"class="hero-area height-600 bg-img background-overlay" style="background-image: url(/storage/thumbnails/{{$post->thumbnail}});">
    <div class="container h-100">
        <div class="row h-100 align-items-center justify-content-center">
            <div class="col-12 col-md-8 col-lg-6">
                <div class="single-blog-title text-center">
                    <!-- Catagory -->
                   
                    <h3 id="title"class="editable-tittle">{{$post->title}}</h3>
                </div>
            </div>
        
        </div>
        <div class="form-group upload-image">
    

        
<form  enctype="multipart/form-data" id="form" method="post" action="{{action('UserActivityController@updatePost') }}" >
    @csrf

    <input type="hidden" name="meta" value="{{$post->meta}}">
    <input id="titleInput" type="text" name="judul" style="display: none">
    <textarea id="contentInput" name="content" style="display: none"></textarea>
    
    <label class="label-image float-right"for="imageInput" style=""> <h6 style="color: white; position: relative; top: -30px;"><i class="fas fa-images"></i>Upload Thumbnail <h6></label>

        <input data-preview="#preview" name="thumbnail" type="file" id="imageInput" onchange="readURL(this)"
            style="display: none"
        >


</form>

@include('inc.messages')
 

        </div>
    </div>

</div>
<script>
    var editor = new MediumEditor('.editable-tittle', {
    keyboardCommands: false,    
    toolbar: false,
    disableReturn:true,
    maxLength : 20,

    placeholder: {
        text: 'Title '
    }
    });
</script>
<!-- ********** Hero Area End ********** -->

<div class="main-content-wrapper section-padding-100">
    <div class="container">
        <div class="row justify-content-center">
            <!-- ============= Post Content Area ============= -->
            <div class="col-12 col-lg-8">
                <div class="single-blog-content mb-100">
                    <!-- Post Meta -->
                  
                    <!-- Post Content -->


                   <style type="text/css">
                       
                            .editor *{
                                        color: black;

                            }
                            .editor p
                            {
                                font-size: 20px;
                            }

                   </style>
                   
                    <div id="content" class="post-content editor" style="color: black">
                       
                        {!!$post->description!!}
                    </div>
                </div>
                <div class="container float-right" style="opacity: 0.6">
               

                <a id="save" onmouseover=""> <h4 class="float-right" style="border: 1px solid black; border-radius: 20px; padding: 8px;">
                    <i class="fas fa-save"></i> Save</h4>
                </a>

              
                </div>
            </div>

        </div>

    </div>

</div>


<script type="text/javascript">

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#bg')
                .css("background-image", 'url('+e.target.result+')')


                     console.log("url : "+e.target.result);  
             };



        reader.readAsDataURL(input.files[0]);
    }
}

var editor2 = new MediumEditor('.editor', {
// options go here
 toolbar: {
                buttons: ['bold', 'italic', 'underline', 'anchor', 'h1', 'h2', ,'h3','quote'],
                
            },
             placeholder: {
    text: 'Tell your story...'
        }

});

 
$('#save').click(function () {
    var title = $('#title').html();
    var content = $('#content').html();

    $('#titleInput').val(title);
    $('#contentInput').val(content);

    $("#form").submit();
    // $('#hiddeninput').append(mysave);
    // alert($('#hiddeninput').val());
});

 

</script>

@endsection