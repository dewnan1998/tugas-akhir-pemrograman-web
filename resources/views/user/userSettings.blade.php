@extends('layouts.app')

@section('styles')
 <style>
     a:hover
     {
         cursor: pointer;
     }
 </style>   
@endsection
@section('content')

<div class="container">

        {{-- enter spacing --}}
        <div class="row mt-5 w-100"></div>

        <div class="container mt-5  mb-5  col-10"> 
        @include('inc.messages')            
            
            <h1  class="mb-5" style="font-family: KievitBold opacity: 0.7">Settings</h1>
        <div class="">
            <h2 class="border-bottom pb-2 mb-3"   style="font-family: KievitBold opacity: 0.7">Account Settings</h2>

            <form id="form" action="/me/settings/update/" method="post">

                @csrf

                <div class="form-group mb-2 border-bottom pb-5">
                <h6 style="font-family: KievitBold;opacity: 0.7">Your Email</h6>
                 <input maxlength="30" id="editEmail"type="email" name="email" class="pr-2 pl-2" value="{{$user->email}}" readonly style="width: auto; max-width: 400px;">
                <div id="buttonEmail">
                    <a class="float-right p-1 border " style="color:#555" onclick="editEmail()">Edit Email</a>
                </div>
            
                </div>
                <div class="form-group pb-5 border-bottom ">
                        <h6 style="font-family: KievitBold;opacity: 0.7">Username</h6>
                        <span>https://dewa.kuy.web.id/@</span>
                        <input  maxlength="16" id="editUsername"type="email" name="username" class="pr-2 pl-2" value="{{$user->username}}" readonly style="width: auto; max-width: 400px;">
                        <div id="buttonUsername">
                        <a class="float-right p-1 border " style="color:#555" onclick="editUsername()">Edit Username</a>
                        </div>
                    </div>
                </form>
        </div>
        </div>    
</div>  
</div>  

<script>
    function editUsername()
    {
        $("#editEmail").prop('readonly', true);
        $("#editEmail").val("{{$user->email}}");
        $("#editUsername").prop('readonly', false); 
        $("#buttonUsername").html('<a class="float-right p-1 border " style="color:#555" onclick="cancel()" >Cancel</a>  <a class="float-right p-1 border mr-2 " style="color:green;border-color:green" onclick="save()" >Save</a> ');
        $("#buttonEmail").html('<a class="float-right p-1 border " style="color:#555" onclick="editEmail()">Edit Email</a> ');

    }

    function editEmail()
    {
        $("#editUsername").prop('readonly', true);
        $("#editUsername").val("{{$user->username}}");
        $("#editEmail").prop('readonly', false); 
        $("#buttonEmail").html('<a class="float-right p-1 border " style="color:#555" onclick="cancel()" >Cancel</a>  <a class="float-right p-1 border mr-2 " style="color:green;border-color:green" onclick="save()" >Save</a> ');
        $("#buttonUsername").html('<a class="float-right p-1 border " style="color:#555" onclick="editUsername()">Edit Username</a> ');

    }

    function cancel()
    {
        $("#editUsername").prop('readonly', true);
        $("#editUsername").val("{{$user->username}}");
        $("#buttonUsername").html('<a class="float-right p-1 border " style="color:#555" onclick="editUsername()">Edit Username</a> ');
             
        $("#editEmail").prop('readonly', true);
        $("#editEmail").val("{{$user->email}}");
        $("#buttonEmail").html('<a class="float-right p-1 border " style="color:#555" onclick="editEmail()">Edit Email</a> ');


    }

    function save()
    {
        $('#form').submit();
    }

</script>

@endsection