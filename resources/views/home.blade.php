@extends('layouts.app')

@section('styles')
    <style>
        a:hover{
            cursor: pointer;
        }
        .par
        {
            height: 550px; 

            /* Create the parallax scrolling effect */
            
            background-attachment: fixed;
            
            background-repeat: no-repeat;
            background-size: cover;
            
        }
        @media (min-width: 970px    ) { 
                .fix
            {
                position: fixed;
                bottom:-55px;;right:3.3%; 
                padding: 2.8%;
                overflow-y: scroll;
                 top: 0;
    bottom: 0;
    
            }
       
             
        }
    
        
      
        .headline *
        {
            opacity: 0.8;
            font-family: KievitBold !important;
        }

        
    </style>
@endsection
@section('content')

    <!-- ********** Hero Area Start ********** -->

    <style>
        
    </style>
    {{-- <div class="hero-area ">
<div id="carouselId" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#carouselId" data-slide-to="0" class="active"></li>
        <li data-target="#carouselId" data-slide-to="1"></li>
        <li data-target="#carouselId" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner" role="listbox">
        <div class="carousel-item active">
                <div class="single-hero-slide bg-img background-overlay par " style="background-image: url({{asset('img/blog-img/bgd1.jpg')}})  ;"></div>
                <!-- Single Slide -->
               
    
        </div>
        <div class="carousel-item">
                <div class="single-hero-slide bg-img background-overlay   par" style="background-image: url({{asset('img/my-img/bgd9.jpg')}});"></div>
             </div>
        <div class="carousel-item">
                <div class="single-hero-slide bg-img background-overlay   par" style="background-image: url({{asset('img/my-img/bgd6.jpg')}});"></div>
    
        </div>
    </div>
    <a class="carousel-control-prev" href="#carouselId" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselId" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
 
</div> --}}
        <!-- Hero Slides Area -->
    <div class="hero-area ">
            
        <div class=" hero-slides owl-carousel" style="">
            
            <!-- Single Slide -->
            <div class="single-hero-slide bg-img background-overlay  " style="background-image: url({{asset('img/blog-img/bgd1.jpg')}});  height: 400px ;"></div>
            <!-- Single Slide -->
            <div class="single-hero-slide bg-img background-overlay  " style="background-image: url({{asset('img/my-img/bgd9.jpg')}}); height: 400px ;"></div>
            <div class="single-hero-slide bg-img background-overlay " style="background-image: url({{asset('img/my-img/bgd6.jpg')}}); height: 400px ;"></div>
 
        </div>
{{--       
        <div class="alert alert-danger mx-auto" style="position: absolute; top: 200px; left: 29%">
            <strong>Danger!</strong> Indicates a dangerous or potentially negative action.
          </div> --}}
        <!-- Hero Post Slide -->
    
    </div>
    <!-- ********** Hero Area End ********** -->

    <div class="main-content-wrapper section-padding-100">
        <div class="container">
            <div class="row ">
                <!-- ============= Post Content Area Start ============= -->
                <div class="col-12 col-lg-8">
                    <div class="post-content-area mb-50">
                        <!-- Catagory Area -->
                        <div class="world-catagory-area">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                {{-- <li class="title">Don’t Miss</li> --}}

                                <li class="nav-item">
                                    <a class="nav-link active" id="tab1" data-toggle="tab" href="#world-tab-1" role="tab" aria-controls="world-tab-1" aria-selected="true">All</a>
                                </li>
                            </ul>

                            <div class="tab-content" id="myTabContent">

                                <div class="tab-pane fade show active" id="world-tab-1" role="tabpanel" aria-labelledby="tab1">
                                    <div class="row">


                                        <div class="col-12 col-md-6">
                                        
                                            <div class="world-catagory-slider owl-carousel wow fadeInUpBig" data-wow-delay="0.1s">

                                                <!-- Single Blog Post -->
                                                
                                                @foreach($posts->shuffle()->take(3) as $post)
                                                <div class="single-blog-post" style="box-shadow: none">
                                                        <!-- Post Thumbnail -->
                                                        <div class="post-thumbnail">
                                                              
                                                            <img src="/storage/thumbnails/{{$post->thumbnail}}" alt=""
                                                                style="object-fit: cover; height: 200px"
                                                            >
                                                            <!-- Catagory -->
                                                            {{-- <div class="post-cta"><a href="#">travel</a></div> --}}
                                                        </div>
                                                        <!-- Post Content -->
                                                        <div class="post-content">
                                                            <a href="/p/{{$post->meta}}" class="headline">
                                                                <h5>{{$post->title}}</h5>
                                                            </a>
                                                            <p class="">{{
                                                                    strip_tags(substr($post->description,0,170))}}
                                                                    @if(strlen($post->description) >170)
                                                                         {{ ' ...'}}
                                                                    @endif
                                                            </p>   <!-- Post Meta -->
                                                            <div class="post-meta">
                                                              
                                        <p><a href="/{{'@'.$post->user->username}}" class="post-author">{{$post->user->name}}</a> on <a href="#" class="post-date">{{$post->created_at->format('M j, Y')}} . {{$post->created_at->diffForHumans()}}</a></p>
                                         </div>
                                                        </div>
                                                    </div>
    
                                                @endforeach
                                                
                                                   
                                             </div>
                                        </div>

                                        <div class="col-12 col-md-6">
                                            <!-- Single Blog Post -->
                                            
                                            <!-- Single Blog Post -->
                                            @foreach($posts->shuffle()->take(4) as $post)

                                            <div class="single-blog-post post-style-2 d-flex align-items-center wow fadeInUpBig" data-wow-delay="0.5s" style="box-shadow: none">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                              
                                                        <img src="/storage/thumbnails/{{$post->thumbnail}}" alt=""
                                                            style="object-fit: cover; height: 110px; width: 200px"
                                                        >
                                                        <!-- Catagory -->
                                                        {{-- <div class="post-cta"><a href="#">travel</a></div> --}}
                                                    </div>
                                                    <!-- Post Content -->
                                                    <div class="post-content" style="overflow-wrap: break-word;overflow: hidden">
                                                        <a href="/p/{{$post->meta}}" class="headline">
                                                            <h5>{{$post->title}}</h5>
                                                        </a>
                                                        <p class="">{{
                                                                strip_tags(substr($post->description,0,60))}}
                                                                @if(strlen($post->description) >60)
                                                                     {{ ' ...'}}
                                                                @endif
                                                        </p>   <!-- Post Meta -->
                                                        <div class="post-meta">
                                                          
                                    <p><a href="/{{'@'.$post->user->username}}" class="post-author">{{$post->user->name}}</a> on <a href="#" class="post-date">{{$post->created_at->format('M j, Y')}} . {{$post->created_at->diffForHumans()}}</a></p>
                                     </div>
                                                    </div>
                                            </div>
                                            @endforeach
                                           
                                       
                                        </div>
                                       
                                    </div>
                                </div>
                               
                                



                            </div>
                        </div>

                        <!-- Catagory Area -->
                        


                    </div>
                </div>

                <!-- ========== Sidebar Area ========== -->
                <div class="col-12 col-md-8 col-lg-4  " style=" ">
                    <div class="post-sidebar-area wow fadeInUpBig border-0  " data-wow-delay="0.2s">
                        <!-- Widget Area -->
                  
                        <!-- Widget Area -->
                        <div  id="sidebar"  class="sidebar-widget-area" style=" ">
                            <h5 class="title">Popular on Dew World</h5>
                            <div class="widget-content">
                                <!-- Single Blog Post -->
                                 
                                @foreach ($posts->take(6) as $index => $post)
                                
                                <div class="single-blog-post post-style-2 d-flex   widget-post" style="
                                box-shadow: none">
                                        <!-- Post Thumbnail -->
                                        <div class="post-thumbnail p-1 pr-0 mr-0">
                                            {{-- <img src="storage/thumbnails/{{$post->thumbnail}}" alt="" style="object-fit: cover; height:80px;"> --}}
                                            <h3 style="opacity: 0.4;">0{{$index+1}}</h3>
                                        </div>
                                        <!-- Post Content -->
                                        <div class="post-content p-1">
                                            <a href="/p/{{$post->meta}}" class="headline">
                                                <h5 class="mb-0" style="font-size: 18px;">{{$post->title}} </h5>
                                                
                                            </a>
                                        <a href="/{{'@'.$post->user->username}}"><p class="mb-0" style="font-size:13px;">{{$post->user->name}}</p></a>
                                                <p  style="font-size:13px;"> <a href="#" class="post-date">{{$post->created_at->format('M j, Y')}} . {{$post->created_at->diffForHumans()}}</a></p>
                                   

                                        </div>
                                    </div>    
                                @endforeach
                                <footer class="border-top">

                                    <ul class="list-inline">
                                            <li class="list-inline-item"><a style="opacity: 0.5"> Help</a></li>
                                            <li class="list-inline-item"><a style="opacity: 0.5"> Status</a></li>
                                            <li class="list-inline-item"><a style="opacity: 0.5"> Writes</a></li>
                                            <li class="list-inline-item"><a style="opacity: 0.5"> Blog</a></li>
                                            <li class="list-inline-item"><a style="opacity: 0.5"> Carrers</a></li>
                                            <li class="list-inline-item"><a style="opacity: 0.5"> Privacy</a></li>
                                            <li class="list-inline-item"><a style="opacity: 0.5"> Terms</a></li>
                                            <li class="list-inline-item"><a style="opacity: 0.5"> About</a></li>
                                        </ul>
                                </footer>
                             
                                
                           
                            </div>
                           
                        </div>
                        <!-- Widget Area -->
                        
                        <!-- Widget Area -->
                        
                            
                    </div>
                </div>
            </div>









            {{-- Lastest Artive --}}
            
            <div class="world-latest-articles" style="">
                <div class="row" style="">
                    <div class="col-12 col-lg-8  ">
                        <div class="title">
                            <h5>Latest Articles</h5>
                        </div>

                        <!-- Single Blog Post -->
                        @foreach ($posts as $post )
                            <div class="mb-4  row" data-wow-delay="0.2s"
                                style="box-shadow: none; padding:0">
                                    <!-- Post Thumbnail -->
                                   
                                    <!-- Post Content -->
                                    <div class="post-content col-8" style="">
                                    <a href="/p/{{$post->meta}}" class="headline">
                                            <h3 class="mb-0">{{$post->title}}</h3>
                                        </a>
                                        <p class="mb-3">{{
                                                strip_tags(substr($post->description,0,170))}}
                                                @if(strlen($post->description) >170)
                                                     {{ ' ...'}}
                                                @endif
                                        </p>
                                        <!-- Post Meta -->
                                        <div class="post-meta">
                                            <p><a style="color: black" href="/{{'@'.$post->user->username}}"  class="post-author">{{$post->user->name}}</a> </p> <p><a href="#" class="">{{$post->created_at->format('M j, Y')}} . {{$post->created_at->diffForHumans()}}</a></p>
                                        </div>
                                    </div>
                                    <div class="post-thumbnail float-right col-4">
                                            <img src="/storage/thumbnails/{{$post->thumbnail}}" alt="" style="object-fit: cover; height: 150px;width:130px; ">
                                        </div>
                                </div>    
                        @endforeach
                        

                        <!-- Single Blog Post -->
                        <!-- Single Blog Post -->
                       
            
                </div>
            </div>

            <!-- Load More btn -->
            {{-- <div class="row">
                <div class="col-12">
                    <div class="load-more-btn mt-50 text-center">
                        <a href="#" class="btn world-btn">Load More</a>
                    </div>
                </div>
            </div> --}}
        </div>
    </div>

   
    </div>

    <script>
        var lastScrollTop = 0;
        var awalTop = $("#sidebar").offset().top;
        $(window).scroll( function(e) {
        
     var st = $(this).scrollTop();
   if (st > lastScrollTop){
    var actualBottom = $("#sidebar").offset().top + $("#sidebar").outerHeight(true);
            
             
     if (document.scrollingElement.scrollTop+$(window).height()  > actualBottom) {
         
        $('#sidebar').css('top',  +document.scrollingElement.scrollTop+($(window).height()/4)-$("#sidebar").height() );
        // $('$sidebar').addClass('fix');
     } 

   } else {
    var top = $("#sidebar").offset().top;
           
             if ($(document).scrollTop() -$("#sidebar").height()/3-10< awalTop) {
                $('#sidebar').css('top', 0);
             } 
             
             else if (document.scrollingElement.scrollTop+$("#sidebar").height()/4-110 < top) {
                $('#sidebar').css('top', document.scrollingElement.scrollTop-$("#sidebar").height()/2-50);
             } 

             

   }
   lastScrollTop = st;
    
   
 
  });
        </script>
    @endsection