@extends('layouts.app')

@section('styles')
<link rel="stylesheet" type="text/css" href="{{asset('css/show-post.css')}}">
<style type="text/css">
                       
   
    .content-post p
    {
        font-size: 20px;
        color: black;
    }
    .headline *
    {
        opacity: 0.8;
            font-family: KievitBold !important; 
  }
 
    h1, h2, h3
    {
        opacity: 1;
            font-family: KievitBold !important;
    }
    p{
        font-weight: normal;
            opacity: 0.8;
            font-family: Charter;
    }

</style>

<script src="https://cdn.jsdelivr.net/npm/medium-editor@latest/dist/js/medium-editor.min.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/medium-editor@latest/dist/css/medium-editor.min.css" type="text/css" media="screen" charset="utf-8">
@endsection

{{-- @if(empty($post->thumbnail))
<img class="w-100" src="//placehold.it/300" style="max-width: 720px"> --}}
 
@section('content')

<div class="hero-area height-400 bg-img background-overlay" style="background-image: url(/storage/thumbnails/{{$post->thumbnail}});">
    <div class="container h-100">
        <div class="row h-100 align-items-center justify-content-center">
            <div class="col-12 col-md-8 col-lg-6">
                <div class="single-blog-title text-center">
                    <!-- Catagory -->
                    {{-- <div class="post-cta"><a href="#">travel</a></div> --}}
                    <h3>{{$post->title}}</h3>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ********** Hero Area End ********** -->

<div class="main-content-wrapper section-padding-100 mb-0 pb-0">
    <div class="container">
        <div class="row justify-content-center">
            <!-- ============= Post Content Area ============= -->
            <div class="col-12 col-lg-8">
                <div class="single-blog-content mb-100">
                    <!-- Post Meta -->
                    <div class="post-meta border-0 ml-5 mb-0" id="follow_div_author">
                        {{-- <p><a href="#" class="post-author">{{$post->user->name}}</a> on <a href="#" class="post-date">{{$post->created_at}}</a></p> --}}
                      @include('inc.author')
                    </div>
                    <!-- Post Content -->
                    <div class="post-content content-post border-0 mt-1 pt-5 " style="box-shadow: none">
                        
                    <h5 style="font-size: 20px !important;">{!!$post->description!!}</h5>
            
                        <ul class="post-tags">
                            <li><a href="#">Manual</a></li>
                            <li><a href="#">Liberty</a></li>
                            <li><a href="#">Recommendations</a></li>
                            <li><a href="#">Interpritation</a></li>
                        </ul>



                        <form action="/user" method="post">
                            @csrf
                            <input type="hidden" name="message" value="{!!$post->description!!}">
                            <button type="submit"> SHARE ON FACEBOOK </button>
                        </form>
                        <div class="border-top mt-3">
                                <div class="d-inline-flex">
                                        <i class="fa fa-comments" aria-hidden="true"></i>
                                </div>
                        </div>
                        <!-- Post Meta -->
                        
                        {{-- <div class="post-meta second-part">
                            <p><a href="#" class="post-author">{{$post->user->name}}</a> on <a href="#" class="post-date">{{$post->created_at}}</a></p>
                            @include('inc.author');
                        </div> --}}
                    </div>
                </div>
            </div>
            
            {{-- <div class="col-12 col-md-8 col-lg-4">
                <div class="post-sidebar-area mb-100">
                    <!-- Widget Area -->
                    <div class="sidebar-widget-area">
                        <h5 class="title">About World</h5>
                        <div class="widget-content">
                            <p>The mango is perfect in that it is always yellow and if it’s not, I don’t want to hear about it. The mango’s only flaw, and it’s a minor one, is the effort it sometimes takes to undress the mango, carve it up in a way that makes sense, and find its way to the mouth.</p>
                        </div>
                    </div>
                    <!-- Widget Area -->
                    <div class="sidebar-widget-area">
                        <h5 class="title">Recent Post</h5>
                        <div class="widget-content">
                            <!-- Single Blog Post -->
                            <div class="single-blog-post post-style-2 d-flex align-items-center widget-post">
                                <!-- Post Thumbnail -->
                                <div class="post-thumbnail">
                                    <img src="img/blog-img/b11.jpg" alt="">
                                </div>
                                <!-- Post Content -->
                                <div class="post-content">
                                    <a href="#" class="headline">
                                        <h5 class="mb-0">How Did van Gogh’s Turbulent Mind Depict One of the Most</h5>
                                    </a>
                                </div>
                            </div>
                            <!-- Single Blog Post -->
                            <div class="single-blog-post post-style-2 d-flex align-items-center widget-post">
                                <!-- Post Thumbnail -->
                                <div class="post-thumbnail">
                                    <img src="img/blog-img/b13.jpg" alt="">
                                </div>
                                <!-- Post Content -->
                                <div class="post-content">
                                    <a href="#" class="headline">
                                        <h5 class="mb-0">How Did van Gogh’s Turbulent Mind Depict One of the Most</h5>
                                    </a>
                                </div>
                            </div>
                            <!-- Single Blog Post -->
                            <div class="single-blog-post post-style-2 d-flex align-items-center widget-post">
                                <!-- Post Thumbnail -->
                                <div class="post-thumbnail">
                                    <img src="img/blog-img/b14.jpg" alt="">
                                </div>
                                <!-- Post Content -->
                                <div class="post-content">
                                    <a href="#" class="headline">
                                        <h5 class="mb-0">How Did van Gogh’s Turbulent Mind Depict One of the Most</h5>
                                    </a>
                                </div>
                            </div>
                            <!-- Single Blog Post -->
                            <div class="single-blog-post post-style-2 d-flex align-items-center widget-post">
                                <!-- Post Thumbnail -->
                                <div class="post-thumbnail">
                                    <img src="img/blog-img/b10.jpg" alt="">
                                </div>
                                <!-- Post Content -->
                                <div class="post-content">
                                    <a href="#" class="headline">
                                        <h5 class="mb-0">How Did van Gogh’s Turbulent Mind Depict One of the Most</h5>
                                    </a>
                                </div>
                            </div>
                            <!-- Single Blog Post -->
                            <div class="single-blog-post post-style-2 d-flex align-items-center widget-post">
                                <!-- Post Thumbnail -->
                                <div class="post-thumbnail">
                                    <img src="img/blog-img/b12.jpg" alt="">
                                </div>
                                <!-- Post Content -->
                                <div class="post-content">
                                    <a href="#" class="headline">
                                        <h5 class="mb-0">How Did van Gogh’s Turbulent Mind Depict One of the Most</h5>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Widget Area -->
                    <div class="sidebar-widget-area">
                        <h5 class="title">Stay Connected</h5>
                        <div class="widget-content">
                            <div class="social-area d-flex justify-content-between">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-pinterest"></i></a>
                                <a href="#"><i class="fa fa-vimeo"></i></a>
                                <a href="#"><i class="fa fa-instagram"></i></a>
                                <a href="#"><i class="fa fa-google"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- Widget Area -->
                    {{-- <div class="sidebar-widget-area">
                        <h5 class="title">Today’s Pick</h5>
                        <div class="widget-content">
                            <!-- Single Blog Post -->
                            <div class="single-blog-post todays-pick">
                                <!-- Post Thumbnail -->
                                <div class="post-thumbnail">
                                    <img src="img/blog-img/b22.jpg" alt="">
                                </div>
                                <!-- Post Content -->
                                <div class="post-content px-0 pb-0">
                                    <a href="#" class="headline">
                                        <h5>How Did van Gogh’s Turbulent Mind Depict One of the Most Complex Concepts in Physics?</h5>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div> --}}
                {{-- </div>
            </div> --}} 
        </div>
    </div>
</div>

<div class="p-5 mt-0" style="background-color: #f9f9f9" >
        <div class="row mb-5 col-lg-10 col-12 mx-auto" style="" >
            
            @foreach($posts as $p)

            <div class="col-12 col-md-6 col-lg-4 " style=" ">
                <!-- Single Blog Post -->
                <div class="single-blog-post  shadow" style="background: white; border-radius: 10px;">
                    <!-- Post Thumbnail -->
                    <div class="post-thumbnail" style=" ">
                        <img src="/storage/thumbnails/{{$p->thumbnail}}" alt="" style=" object-fit: cover; height: 100px;">
                        <!-- Catagory -->
                        <div class="post-cta"><a href="#">travel</a></div>
                    </div>
                    <!-- Post Content -->
                    <div class="post-content" style="overflow-wrap: break-word; overflow: hidden">
                    <a href="/p/{{$p->meta}}" class="headline">
                            <h4>{{$p->title}}</h4>
                        </a>
                        <p>{{
                            strip_tags(substr($p->description,0,170))}}
                            @if(strlen($p->description) >170)
                                 {{ ' ...'}}
                                @endif</p>
                        <!-- Post Meta -->
                        <div class="post-meta">
                                <p><a href="/{{'@'.$post->user->username}}" class="post-author">{{$post->user->name}}</a> on <a href="#" class="post-date">{{$post->created_at->format('M j, Y')}} . {{$post->created_at->diffForHumans()}}</a></p>
                            </div>
                    </div>
                </div>
            </div>

            
            @endforeach
           
            @if(Auth::check())
            <form class="d-none" action="/p/{{$post->meta}}/reply" method="post" id="form">
                @csrf
                <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                <input type="hidden" name="post_id" value="{{$post->id}}">
                <input id="comments" type="hidden" name="comments">
            </form>


            <div class="container col-12 col-lg-10">
            <p class="mt-5 " style="font-size: 16px;">Response</p>

       
        
            <div class="shadow-sm border container p-3" style="background: white;">
                
                <div class="d-inline-flex "> 
                    @include('inc.userAvatar',['user' =>Auth::user()])
                   <p class="ml-3 pt-2" style="font-size: 16px; color: green">{{Auth::user()->name}}</p>
                </div>
            
                <div  id="responseEditor"class="editor  mt-1 p-1" style="color: black">

                </div>
                <div  class=" mt-4" style="">
                    <a  class="p-2" id ="save"style="font-size: 16px; opacity: 0.6; color: green; border:  1px green solid; border-radius: 5px;"> Publish</a>
                    </div>

            </div>
            @endif
            <div class="text-center mt-5">
                @if(count($post->responses)>0)

                {{-- <a class="border p-2" style="border-color: green; color: green"> Show All Responses </a> --}}
           
                @foreach ($post->responses as $response)
                    <div>
                        <div class="shadow-sm  border container p-3 text-left mb-3" style="background: white;">
                
                            <div class="d-inline-flex "> 
                                @include('inc.userAvatar',['user'=>$response->user])
                                 <div class="d-lg-block ml-3">     
                                <p class="p-0 m-0" style="font-size: 16px; color: green">{{$response->user->name}}</p>
                                 <p> <small>{{$response->created_at->format('M d, Y')}}</small></p></div>
                            </div>
                        
                            <div  id=""class="mt-1 p-1" style="">
                                {!!$response->comments!!}
                            </div>
                                        
                        </div>
                    </div>  
                @endforeach
                @else
                @if(Auth::check())<p>Be The first who response </p>
                @endif 
            @endif
                </div>
            
                
        </div>
        </div>
    </div>


    <style>
        .editor p
        {
            color: black;
            font-size: 18px;
        }
    </style>
<script>

$('#save').click(function () {

    var content = $('#responseEditor').html();

   console.log(content)
    $('#comments').val(content);

    $("#form").submit();
    // $('#hiddeninput').append(mysave);
    // alert($('#hiddeninput').val());
});

var editor = new MediumEditor('.editor', {
// options go here
 toolbar: {
                buttons: ['bold', 'italic', 'underline', 'anchor', ,'h3','quote'],
                
            },
             placeholder: {
    text: 'Enter response...'
        }

});

 
    function follow( username){
    
    
        // var _username = $('#usernameFollow').val();
        var _username = username;
    $.ajax({
    
            type:"POST",
            url:'/@'+_username+'/follow',
                              
            data:{
                username : _username
            },
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
            success:function(responsedata){
                    
                console.log(responsedata);
                $("#follow_div_author").load(location.href+" #follow_div_author>*","");
    
            }
         })
    
    }
    function unfollow( username){
    
    
    var _username = username
    // var _username = $('#usernameFollow').val();

    $.ajax({
    
            type:"POST",
            url:'/@'+_username+'/unfollow',
                              
            data:{
                username : _username
            },
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
            success:function(responsedata){
                    
                console.log(responsedata);
                $("#follow_div_author").load(location.href+" #follow_div_author>*","");
    
            }
         })
    
    }
</script>

@endsection