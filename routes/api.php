<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/login/user' ,'AjaxAuthController@login');
Route::get('/auth/login' ,'APIController@form');
Route::post('/oauth/login' ,'APIController@login'); 
Route::group(['middleware' => 'auth:api'], function () {

       
});

// Route::get('/oauth/login/{callback}' ,'APIController@form');
// Route::post('/oauth/login/{callback}' ,'APIController@login');

Route::group([

    // 'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {

    Route::post('login', 'APIController@login');
    Route::post('logout', 'APIController@logout');
    Route::post('refresh', 'APIController@refresh');
    Route::get('me', 'APIController@me');

});
