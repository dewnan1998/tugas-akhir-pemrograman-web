<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','PagesController@index');

// Route::get('/user/login', function () {
//     return view('auth.login');
// });


Route::post('/login/user' ,'AjaxAuthController@login');


Route::get('/register','PagesController@register')->middleware('guest');
Route::post('/register','AuthController@register');

Route::get('/login','PagesController@login')->name('login');
Route::post('/login','AuthController@login');
Route::get('/logout','AuthController@logout');

Route::post('/register','AuthController@register');


// Route::get('/portal/admin', function () {
//     return 'Anda admin';
// })->middleware('admin');


Route::group(['prefix' => 'portal/admin',  'middleware' => 'admin'], function()
{
    Route::get('/' ,'AdminController@index');
    
    Route::group(['prefix'=> 'manage'] , function()
    {
        Route::resource('/posts' ,'PostController');
        Route::resource('/users' ,'UserController');
    });
    
});

Route::get('messenger' ,'MessengerController@index');
Route::post('messenger/chat' ,'MessengerController@chat');

Route::get('/p/{title}' ,'PagesController@showPost');

Route::get('/@{user}','PagesController@userProfile');
Route::get('/@{user}/following','PagesController@userProfileFollows');
Route::get('/@{user}/followers','PagesController@userProfileFollowers');
Route::get('/@{user}/responses','PagesController@userProfileResponses');
Route::get('/@{user}/claps','PagesController@userProfileClaps');

Route::get('/write', function()
{
    return view('write');
})->middleware('auth');

Route::get('/p/{meta}/edit','UserActivityController@editPost')->middleware('auth','moderator');
Route::post('/p/{meta}/delete','UserActivityController@deletePost')->middleware('auth','moderator');

Route::post('/post/store','UserActivityController@addPost');
Route::post('/post/update','UserActivityController@updatePost');


Route::group(['middleware' => 'auth'], function () {
    
    Route::get('/user', 'GraphController@retrieveUserProfile');
    Route::post('/user', 'GraphController@publishToPage');
    

    Route::post('/@{user}/follow', 'UserActivityController@follow')->name('follow');
    Route::post('/@{user}/unfollow', 'UserActivityController@unfollow')->name('unfollow');
    Route::post('/p/{meta}/reply/', 'UserActivityController@replyPost')->name('reply');

});


Route::get('/getData', 'AuthController@getData');
Route::group(['prefix' => 'me','middleware' =>'auth'],function()
{
    Route::get('/stories','UserActivityController@myPosts');
    Route::get('/settings','UserActivityController@mySettings');
    Route::post('/settings/update','UserActivityController@UpdateSettings');
    Route::get('/followers' ,'UserActivityController@followwer');

    Route::get('/stats' ,'UserActivityController@stats');
    
    Route::get('/' , function(){

        return redirect ('/@'.Auth::user()->username);
        
    });

    // Route::get('/settings' ,'UserActivityController@settings');
    Route::get('/stats/post/{meta}' ,'UserActivityController@statsPost');
    

});


Route::group(['prefix' => '/@{username}','middleware' => ['auth' ,'isMe'] ], function()
{
    Route::get('/edit' ,'UserActivityController@editProfile');
    Route::post('/update','UserActivityController@updateProfile');
    
});



Route::get('auth/{provider}', 'AuthController@redirectToProvider');
Route::get('auth/{provider}/callback', 'AuthController@handleProviderCallback');